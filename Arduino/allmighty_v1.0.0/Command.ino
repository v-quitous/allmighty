void command(String data) {
  data.toLowerCase();
  if (data.indexOf("fire_warning_light") > -1) {
    data = trimLeftCommand(data);
    if (data == "off") {
      fireWarningLight1.off();   // fire_warning_light-off
      fireWarningLight2.off();
    } else if (data.indexOf("flicker") > -1) {
      data = trimLeftCommand(data);
      fireWarningLight1.flicker(atoi(data.c_str()) * MILLISECOND);   // fire_warning_light-flicker-[MILLISECOND]
      fireWarningLight2.flicker(atoi(data.c_str()) * MILLISECOND);
    } else if (data.indexOf("fade") > -1) {
      data = trimLeftCommand(data);
      fireWarningLight1.fade(atoi(data.c_str()) * MILLISECOND);   // fire_warning_light-fade-[MILLISECOND]
      fireWarningLight2.fade(atoi(data.c_str()) * MILLISECOND);
    }
  }

  else if (data.indexOf("door_lock") > -1) {
    data = trimLeftCommand(data);
    if (data == "lock") {   // door_lock-lock
      doorLockGreen.off();
      doorLockRed.on();
    } else if (data == "unlock") {    // door_lock-unlock
      doorLockGreen.on();
      doorLockRed.off();
    } else if (data == "broken") {    // door_lock-broken
      doorLockGreen.off();
      doorLockRed.flicker(1000);
    } else if (data == "idle") {    // door_lock-idle
      doorLockGreen.off();
      doorLockRed.off();
    }
  }
  
  else if (data.indexOf("firewall") > -1) {
    data = trimLeftCommand(data);
    if (data.indexOf("rotate") > -1) {
      data = trimLeftCommand(data);
      firewall.rotate(atoi(data.c_str()));    // firewall-rotate-[ANGLE]  (0 <= ANGLE <= 180)
    } else if (data.indexOf("change_speed") > -1) {
      data = trimLeftCommand(data);
      firewall.changeSpeed(atoi(data.c_str()));   // firewall-change_speed-[SPEED]  (1 <= SPEED <= 100)
    } else if (data == "activate") {
      firewall.rotate(90);    // firewall-activate
    } else if (data == "deactivate") {
      firewall.rotate(180);   // firewall-deactivate
    }
  }

  else if (data.indexOf("window") > -1) {
    data = trimLeftCommand(data);
    if (data.indexOf("rotate") > -1) {
      data = trimLeftCommand(data);
      window.rotate(atoi(data.c_str()));    // window-rotate-[ANGLE]  (0 <= ANGLE <= 180)
    } else if (data.indexOf("change_speed") > -1) {
      data = trimLeftCommand(data);
      window.changeSpeed(atoi(data.c_str()));   // window-change_speed-[SPEED]  (1 <= SPEED <= 100)
    } else if (data == "open") {
      window.rotate(30);    // window-open
    } else if (data == "close") {
      window.rotate(0);   // window-close
    }
  }

  else if (data.indexOf("parking_gate") > -1) {
    data = trimLeftCommand(data);
    if (data.indexOf("rotate") > -1) {
      data = trimLeftCommand(data);
      parkingGate.rotate(atoi(data.c_str()));    // parking_gate-rotate-[ANGLE]  (0 <= ANGLE <= 180)
    } else if (data.indexOf("change_speed") > -1) {
      data = trimLeftCommand(data);
      parkingGate.changeSpeed(atoi(data.c_str()));   // parking_gate-change_speed-[SPEED]  (1 <= SPEED <= 100)
    } else if (data == "open") {
      parkingGate.rotate(90);    // parking_gate-open
    } else if (data == "close") {
      parkingGate.rotate(0);   // parking_gate-close
    }
  }
  
  else if (data.indexOf("elevator") > -1) {
    data = trimLeftCommand(data);
    if (data.indexOf("move") > -1) {
      data = trimLeftCommand(data);
      if (data.indexOf("-") > -1) {   // elevator-move-[STEP]-[DIRECTION]  (DIRECTION = {UP, DOWN})
        long stp = atoi(trimRightCommand(data).c_str());
        data = trimLeftCommand(data);
        int dir = data == "up" ? HIGH : LOW;
        elevator.rotate(stp, dir);
      } else {    // elevator-move-[DIRECTION]  (DIRECTION = {UP, DOWN})
        data = trimLeftCommand(data);
        int dir = data == "up" ? HIGH : LOW;
        elevator.rotate(dir);
      }
    } else if (data.indexOf("stop") > -1) {   // elevator-stop
      elevator.stop();
    } else if (data.indexOf("change_speed") > -1) {   // elevator-change_speed-[SPEED]  (1 <= SPEED <= 100)
      data = trimLeftCommand(data);
      elevator.changeSpeed(atoi(data.c_str()));
    }
  }

  else if (data.indexOf("lobby_light_right") > -1) {
    data = trimLeftCommand(data);
    if (data == "off") {    // lobby_light_right-off
      lobbyLightRight.off();
    } else if (data == "on") {    // lobby_light_right-on
      lobbyLightRight.on();
    } else if (data.indexOf("flicker") > -1) {
      data = trimLeftCommand(data);
      lobbyLightRight.flicker(atoi(data.c_str()) * MILLISECOND);    // lobby_light_right-flicker-[MILLISECOND]
    } else if (data.indexOf("fade") > -1) {
      data = trimLeftCommand(data);
      lobbyLightRight.fade(atoi(data.c_str()) * MILLISECOND);     // lobby_light_right-fade-[MILLISECOND]
    } else if (data.indexOf("set_color") > -1) {
      data = trimLeftCommand(data);
      int index = atoi(trimRightCommand(data).c_str());
      data = trimLeftCommand(data);
      int red = atoi(trimRightCommand(data).c_str());
      data = trimLeftCommand(data);
      int green = atoi(trimRightCommand(data).c_str());
      data = trimLeftCommand(data);
      int blue = atoi(data.c_str());
      lobbyLightRight.setColor(index, red, green, blue);    // lobby_light_right-set_color-[index]-[red]-[green]-[blue]
    } else if (data.indexOf("set_whole_color") > -1) {
      data = trimLeftCommand(data);
      int red = atoi(trimRightCommand(data).c_str());
      data = trimLeftCommand(data);
      int green = atoi(trimRightCommand(data).c_str());
      data = trimLeftCommand(data);
      int blue = atoi(data.c_str());
      lobbyLightRight.setColor(-1, red, green, blue);   // lobby_light_right-set_whole_color-[red]-[green]-[blue]
    }
  }

  else if (data.indexOf("lobby_light_left") > -1) {
    data = trimLeftCommand(data);
    if (data == "off") {    // lobby_light_left-off
      lobbyLightLeft.off();
    } else if (data == "on") {    // lobby_light_left-on
      lobbyLightLeft.on();
    } else if (data.indexOf("flicker") > -1) {
      data = trimLeftCommand(data);
      lobbyLightLeft.flicker(atoi(data.c_str()) * MILLISECOND);    // lobby_light_left-flicker-[MILLISECOND]
    } else if (data.indexOf("fade") > -1) {
      data = trimLeftCommand(data);
      lobbyLightLeft.fade(atoi(data.c_str()) * MILLISECOND);     // lobby_light_left-fade-[MILLISECOND]
    } else if (data.indexOf("set_color") > -1) {
      data = trimLeftCommand(data);
      int index = atoi(trimRightCommand(data).c_str());
      data = trimLeftCommand(data);
      int red = atoi(trimRightCommand(data).c_str());
      data = trimLeftCommand(data);
      int green = atoi(trimRightCommand(data).c_str());
      data = trimLeftCommand(data);
      int blue = atoi(data.c_str());
      lobbyLightLeft.setColor(index, red, green, blue);    // lobby_light_left-set_color-[index]-[red]-[green]-[blue]
    } else if (data.indexOf("set_whole_color") > -1) {
      data = trimLeftCommand(data);
      int red = atoi(trimRightCommand(data).c_str());
      data = trimLeftCommand(data);
      int green = atoi(trimRightCommand(data).c_str());
      data = trimLeftCommand(data);
      int blue = atoi(data.c_str());
      lobbyLightLeft.setColor(-1, red, green, blue);   // lobby_light_left-set_whole_color-[red]-[green]-[blue]
    }
  }

  else if (data.indexOf("room_light") > -1) {
    data = trimLeftCommand(data);
    if (data == "on") {   // room_light-on
      cultivatorLeft.on();
    } else if (data == "off") {   // room_light-off
      cultivatorLeft.off();
    } else if (data.indexOf("change_brightness") > -1) {
      data = trimLeftCommand(data);
      cultivatorLeft.changeBrightness(atoi(data.c_str()));    // room_light-change_brightness-[BRIGHTNESS]  (0 <= BRIGHTNESS <= 255)
    }
  }

  else if (data.indexOf("cultivator") > -1) {
    data = trimLeftCommand(data);
    if (data.indexOf("left") > -1) {
      data = trimLeftCommand(data);
      if (data == "off") {    // cultivator-left-off
        roomLight.off();
      } else if (data == "on") {    // cultivator-left-on
        roomLight.on();
      } else if (data.indexOf("flicker") > -1) {
        data = trimLeftCommand(data);
        roomLight.flicker(atoi(data.c_str()) * MILLISECOND);    // cultivator-left-flicker-[MILLISECOND]
      } else if (data.indexOf("fade") > -1) {
        data = trimLeftCommand(data);
        roomLight.fade(atoi(data.c_str()) * MILLISECOND);     // cultivator-left-fade-[MILLISECOND]
      } else if (data.indexOf("set_color") > -1) {
        data = trimLeftCommand(data);
        int index = atoi(trimRightCommand(data).c_str());
        data = trimLeftCommand(data);
        int red = atoi(trimRightCommand(data).c_str());
        data = trimLeftCommand(data);
        int green = atoi(trimRightCommand(data).c_str());
        data = trimLeftCommand(data);
        int blue = atoi(data.c_str());
        roomLight.setColor(index, red, green, blue);    // cultivator-left-set_color-[index]-[red]-[green]-[blue]
      } else if (data.indexOf("set_whole_color") > -1) {
        data = trimLeftCommand(data);
        int red = atoi(trimRightCommand(data).c_str());
        data = trimLeftCommand(data);
        int green = atoi(trimRightCommand(data).c_str());
        data = trimLeftCommand(data);
        int blue = atoi(data.c_str());
        roomLight.setColor(-1, red, green, blue);   // cultivator-left-set_whole_color-[red]-[green]-[blue]
      }
    } else if (data.indexOf("right") > -1) {
      data = trimLeftCommand(data);
      if (data == "on") {   // cultivator-right-on
        cultivatorRight.on();
      } else if (data == "off") {   // cultivator-right-off
        cultivatorRight.off();
      } else if (data.indexOf("change_intensity") > -1) {
        data = trimLeftCommand(data);
        cultivatorRight.changeBrightness(atoi(data.c_str()));    // cultivator-right-change_intensity-[INTENSITY]  (0 <= INTENSITY <= 255)
      }
    }
  }
}

String trimLeftCommand(String c) {
  return c.substring(c.indexOf("-") + 1, c.length());
}

String trimRightCommand(String c) {
  return c.substring(0, c.indexOf("-"));
}
