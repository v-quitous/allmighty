#ifndef SmartBulb_h
#define SmartBulb_h

#include "Arduino.h"
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

class SmartBulb {
  public:
    void setup(int pin, int num, Adafruit_NeoPixel* pixels);
    void loop();
    void off();
    void on();
    void flicker(long interval);
    void fade(long interval);
    void setColor(int index, int red, int green, int blue);
    boolean isActivated();

  private:
    Adafruit_NeoPixel* _pixels;
    int _mode;
    int _pmode;
    int _pin;
    int _num;
    int _voltage;
    int _color[64][3];
    long _cnt;
    long _interval;
    boolean _activated;
    boolean _colorChanged;
};

#endif
