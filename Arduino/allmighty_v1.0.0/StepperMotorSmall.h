#ifndef StepperMotorSmall_h
#define StepperMotorSmall_h

#include "Arduino.h"

class StepperMotorSmall {
  public:
    void setup(int pins[4]);
    void loop();
    void rotate(int dir);
    void rotate(long stp, int dir);
    void stop();
    void changeSpeed(int s);
  private:
    int _pin1, _pin2, _pin3, _pin4;
    long _stp;
    long _stpCount;
    int _dir;
    int _speed;
    int _speedCount;
    void _rotate();
    void _digitalWrite(int bit4);
};

#endif
