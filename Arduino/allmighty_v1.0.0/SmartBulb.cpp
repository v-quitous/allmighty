#include "SmartBulb.h"

const int OFF = 0, ON = 1, FLICKER = 2, FADE = 3;

void SmartBulb::setup(int pin, int num, Adafruit_NeoPixel* pixels) {
  _mode = OFF;
  _pmode = OFF;
  _pin = pin;
  _num = num;
  _voltage = LOW;
  _cnt = 0;
  _interval = 0;
  _activated = false;
  _colorChanged = false;
  
  for (int i = 0; i < num; i++) {
    _color[i][0] = 0;
    _color[i][1] = 0;
    _color[i][2] = 0;
  }
    
  _pixels = pixels;
  _pixels->begin();
  _pixels->clear();
  _pixels->show();
}

void SmartBulb::loop() {
  _pixels->clear();
  _pixels->setBrightness(255);
  switch (_mode) {
    case OFF:
      _cnt = 0;
      _voltage = LOW;
      if (_pmode != _mode) {
        if (_activated) {
          _pixels->show();
          _pmode = _mode;
          _activated = false;
        } else {
          _activated = true;
        }
      }
      break;
    case ON:
      _cnt = 0;
      _voltage = HIGH;
      for (int i = 0; i < _num; i++) {
        _pixels->setPixelColor(i, _pixels->Color(_color[i][0], _color[i][1], _color[i][2]));
      }
      if (_pmode != _mode) {
        if (_activated) {
          _pixels->show();
          _pmode = _mode;
          _colorChanged = false;
          _activated = false;
        } else {
          _activated = true;
        }
      } else if (_colorChanged) {
        if (_activated) {
          _pixels->show();
          _colorChanged = false;
          _activated = false;
        } else {
          _activated = true;
        }
      }
      break;
    case FLICKER:
      _pmode = FLICKER;
      if (_cnt == 0) {
        _voltage = _voltage == LOW ? HIGH : LOW;
        _colorChanged = true;
      }
      _cnt++;
      if (_cnt == _interval) {
        _cnt = 0;
      }
      if (_voltage == HIGH) {
        for (int i = 0; i < _num; i++) {
          _pixels->setPixelColor(i, _pixels->Color(_color[i][0], _color[i][1], _color[i][2]));
        }
      }
      if (_colorChanged) {
        if (_activated) {
          _pixels->show();
          _colorChanged = false;
          _activated = false;
        } else {
          _activated = true;
        }
      }
      break;
    case FADE:
      _pmode = FADE;
      if (_cnt == 0) {
        _voltage = HIGH;
      } else if (_cnt == _interval) {
        _voltage = LOW;
      }
      if (_voltage == HIGH) _cnt++;
      else _cnt--;
      for (int i = 0; i < _num; i++) {
        _pixels->setPixelColor(i, _pixels->Color(_color[i][0], _color[i][1], _color[i][2]));
      }
      _pixels->setBrightness(map(_cnt, 0, _interval, 0, 255));
      _pixels->show();
      break;
  }
}

void SmartBulb::off() {
  _mode = OFF;
}

void SmartBulb::on() {
  _mode = ON;
}

void SmartBulb::flicker(long interval) {
  _mode = FLICKER;
  _cnt = 0;
  _voltage = LOW;
  _interval = interval / 100;
}

void SmartBulb::fade(long interval) {
  _mode = FADE;
  _cnt = 0;
  _interval = interval / 100;
}

void SmartBulb::setColor(int index, int red, int green, int blue) {
  if (index >= _num) return;
  else if (index >= 0) {
    _color[index][0] = red;
    _color[index][1] = green;
    _color[index][2] = blue;
  } else {
    for (int i = 0; i < _num; i++) {
      _color[i][0] = red;
      _color[i][1] = green;
      _color[i][2] = blue;
    }
  }
  _colorChanged = true;
}

boolean SmartBulb::isActivated() {
  return _activated || _mode == FADE;
}
