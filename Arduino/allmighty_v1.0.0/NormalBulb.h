#ifndef NormalBulb_h
#define NormalBulb_h

#include "Arduino.h"

class NormalBulb {
  public:
    void setup(int pin);
    void loop();
    void off();
    void on();
    void flicker(long interval);
    void fade(long interval);
    void changeBrightness(int brightness);

  private:
    int _mode;
    int _pin;
    long _cnt;
    int _voltage;
    long _interval;
    int _brightness;
};

#endif
