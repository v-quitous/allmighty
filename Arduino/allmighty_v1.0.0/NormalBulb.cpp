#include "NormalBulb.h"

const int OFF = 0, ON = 1, FLICKER = 2, FADE = 3;

void NormalBulb::setup(int pin) {
  _mode = OFF;
  _pin = pin;
  _cnt = 0;
  _voltage = LOW;
  _interval = 0;
  _brightness = 255;
  pinMode(_pin, OUTPUT);
}

void NormalBulb::loop() {
  switch (_mode) {
    case OFF:
      _cnt = 0;
      _voltage = LOW;
      digitalWrite(_pin, _voltage);
      break;
    case ON:
      _cnt = 0;
      _voltage = HIGH;
      analogWrite(_pin, _brightness);
      break;
    case FLICKER:
      if (_cnt == 0) {
        _voltage = _voltage == LOW ? HIGH : LOW;
      }
      _cnt++;
      if (_cnt == _interval) {
        _cnt = 0;
      }
      digitalWrite(_pin, _voltage);
      break;
    case FADE:
      if (_cnt == 0) {
        _voltage = HIGH;
      } else if (_cnt == _interval) {
        _voltage = LOW;
      }
      if (_voltage == HIGH) _cnt++;
      else _cnt--;
      analogWrite(_pin, map(_cnt, 0, _interval, 0, 255));
      break;
  }
}

void NormalBulb::off() {
  _mode = OFF;
}

void NormalBulb::on() {
  _mode = ON;
}

void NormalBulb::flicker(long interval) {
  _mode = FLICKER;
  _cnt = 0;
  _voltage = LOW;
  _interval = interval;
}

void NormalBulb::fade(long interval) {
  _mode = FADE;
  _cnt = 0;
  _interval = interval / 5;
}

void NormalBulb::changeBrightness(int brightness) {
  if (brightness > 255) brightness = 255;
  if (brightness < 0) brightness = 0;
  _brightness = brightness;
}
