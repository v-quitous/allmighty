#include "ServoMotor.h"
#include "NormalBulb.h"
#include "StepperMotorSmall.h"
#include "SmartBulb.h"

/* Pins & Objects */
const int RELAY = 52;
const int FIRE_WARNING_LIGHT[2] = {6, 12};
const int DOOR_LOCK_GREEN = 5;
const int DOOR_LOCK_RED = 4;
const int CULTIVATOR[2] = {2, 3};
const int FIREWALL = 8;
const int WINDOW = 13;
const int PARKING_GATE = 11;
const int ELEVATOR[4] = {42, 44, 46, 48};
Adafruit_NeoPixel LOBBY_LIGHT_RIGHT(9, 10, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel LOBBY_LIGHT_LEFT(5, 7, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel ROOM_LIGHT(2, 9, NEO_GRB + NEO_KHZ800);

/* Devices */
NormalBulb fireWarningLight1, fireWarningLight2;
NormalBulb doorLockGreen, doorLockRed;
NormalBulb cultivatorLeft, cultivatorRight;
ServoMotor firewall;
ServoMotor window;
ServoMotor parkingGate;
StepperMotorSmall elevator;
SmartBulb lobbyLightRight, lobbyLightLeft;
SmartBulb roomLight;

/* Constants */
const long SECOND = 117000;
const long MILLISECOND = 117;

void setup() {
  Serial.begin(9600);
  pinMode(RELAY, OUTPUT);
  fireWarningLight1.setup(FIRE_WARNING_LIGHT[0]);
  fireWarningLight2.setup(FIRE_WARNING_LIGHT[1]);
  doorLockGreen.setup(DOOR_LOCK_GREEN);
  doorLockRed.setup(DOOR_LOCK_RED);
  cultivatorLeft.setup(CULTIVATOR[0]);
  cultivatorRight.setup(CULTIVATOR[1]);
  firewall.setup(FIREWALL, 180);
  window.setup(WINDOW, 0);
  parkingGate.setup(PARKING_GATE, 0);
  elevator.setup(ELEVATOR);
  lobbyLightRight.setup(10, 9, &LOBBY_LIGHT_RIGHT);
  lobbyLightLeft.setup(7, 5, &LOBBY_LIGHT_LEFT);
  roomLight.setup(9, 2, &ROOM_LIGHT);
}

void loop() {
  if (Serial.available()) {
    String data = Serial.readString();
    data.trim();
    command(data);
  }
  
  if (lobbyLightRight.isActivated() || lobbyLightLeft.isActivated() || roomLight.isActivated()) digitalWrite(RELAY, LOW);
  else digitalWrite(RELAY, HIGH);
  
  fireWarningLight1.loop();
  fireWarningLight2.loop();
  doorLockGreen.loop();
  doorLockRed.loop();
  cultivatorLeft.loop();
  cultivatorRight.loop();
  firewall.loop();
  window.loop();
  parkingGate.loop();
  elevator.loop();
  lobbyLightRight.loop();
  lobbyLightLeft.loop();
  roomLight.loop();
}
