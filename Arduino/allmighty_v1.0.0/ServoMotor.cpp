#include "ServoMotor.h"

const double INIT_SPEED = 0.01;

void ServoMotor::setup(int pin, int angle) {
  _servo.attach(pin);
  _speed = INIT_SPEED;
  if (angle > 180) angle = 180;
  if (angle < 0) angle = 0;
  _angle = angle;
  _pangle = angle;
  _servo.write(angle);
}

void ServoMotor::loop() {
  if (_pangle == _angle) return;
  if (_pangle < _angle) {
    _pangle += _speed;
    if (_pangle >= _angle) _pangle = _angle;
  } else if (_pangle > _angle) {
    _pangle -= _speed;
    if (_pangle <= _angle) _pangle = _angle;
  }
  _servo.write(_pangle);
}

void ServoMotor::rotate(int angle) {
  if (angle > 180) angle = 180;
  if (angle < 0) angle = 0;
  _angle = angle;
}

void ServoMotor::changeSpeed(double s) {
  if (s < 1) s = 1;
  if (s > 100) s = 100;
  _speed = s / 100;
}
