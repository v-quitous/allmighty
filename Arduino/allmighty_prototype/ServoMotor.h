#ifndef ServoMotor_h
#define ServoMotor_h

#include "Arduino.h"
#include <Servo.h>

class ServoMotor {
  public:
    void setup(int pin);
    void loop();
    void rotate(int angle);
    void changeSpeed(double s);
  private:
    Servo _servo;
    double _speed;
    double _angle;
    double _pangle;
};

#endif
