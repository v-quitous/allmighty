void command(String data) {
  data.toLowerCase();
  if (data.indexOf("fire_warning_light") > -1) {
    data = trimLeftCommand(data);
    if (data == "off") {
      fireWarningLight.off();   // fire_warning_light-off
      fireWarningLight2.off();
    } else if (data.indexOf("flicker") > -1) {
      data = trimLeftCommand(data);
      fireWarningLight.flicker(atoi(data.c_str()) * MILLISECOND);   // fire_warning_light-flicker-[MILLISECOND]
      fireWarningLight2.flicker(atoi(data.c_str()) * MILLISECOND);
    } else if (data.indexOf("fade") > -1) {
      data = trimLeftCommand(data);
      fireWarningLight.fade(atoi(data.c_str()) * MILLISECOND);   // fire_warning_light-fade-[MILLISECOND]
      fireWarningLight2.fade(atoi(data.c_str()) * MILLISECOND);
    }
  }
  
  else if (data.indexOf("firewall") > -1) {
    data = trimLeftCommand(data);
    if (data.indexOf("rotate") > -1) {
      data = trimLeftCommand(data);
      firewall.rotate(atoi(data.c_str()));    // firewall-rotate-[ANGLE]  (0 <= ANGLE <= 180)
    } else if (data.indexOf("change_speed") > -1) {
      data = trimLeftCommand(data);
      firewall.changeSpeed(atoi(data.c_str()));   // firewall-change_speed-[SPEED]  (1 <= SPEED <= 100)
    }
  }
  
  else if (data.indexOf("elevator") > -1) {
    data = trimLeftCommand(data);
    if (data.indexOf("move") > -1) {
      data = trimLeftCommand(data);
      if (data.indexOf("-") > -1) {   // elevator-move-[STEP]-[DIRECTION]  (DIRECTION = {UP, DOWN})
        long stp = atoi(trimRightCommand(data).c_str());
        data = trimLeftCommand(data);
        int dir = data == "up" ? HIGH : LOW;
        elevator.rotate(stp, dir);
      } else {    // elevator-move-[DIRECTION]  (DIRECTION = {UP, DOWN})
        data = trimLeftCommand(data);
        int dir = data == "up" ? HIGH : LOW;
        elevator.rotate(dir);
      }
    } else if (data.indexOf("stop") > -1) {   // elevator-stop
      elevator.stop();
    } else if (data.indexOf("change_speed") > -1) {   // elevator-change_speed-[SPEED]  (1 <= SPEED <= 100)
      data = trimLeftCommand(data);
      elevator.changeSpeed(atoi(data.c_str()));
    }
  }
}

String trimLeftCommand(String c) {
  return c.substring(c.indexOf("-") + 1, c.length());
}

String trimRightCommand(String c) {
  return c.substring(0, c.indexOf("-"));
}
