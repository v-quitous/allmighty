#include "ServoMotor.h"
#include "WarningLight.h"
#include "StepperMotorSmall.h"

/* Pins */
const int FIRE_WARNING_LIGHT[2] = {3, 6};
const int FIREWALL = 5;
const int STEPPER_MOTOR_SMALL[4] = {10, 11, 12, 13};

/* Devices */
WarningLight fireWarningLight, fireWarningLight2;
ServoMotor firewall;
StepperMotorSmall elevator;

/* Constants */
const long SECOND = 117000;
const long MILLISECOND = 117;

void setup() {
  Serial.begin(9600);
  fireWarningLight.setup(FIRE_WARNING_LIGHT[0]);
  fireWarningLight2.setup(FIRE_WARNING_LIGHT[1]);
  firewall.setup(FIREWALL);
  elevator.setup(STEPPER_MOTOR_SMALL);
}

void loop() {
  if (Serial.available()) {
    String data = Serial.readString();
    data.trim();
    command(data);
  }
  
  fireWarningLight.loop();
  fireWarningLight2.loop();
  firewall.loop();
  elevator.loop();
}
