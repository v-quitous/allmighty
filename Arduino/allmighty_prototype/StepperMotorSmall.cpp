#include "StepperMotorSmall.h"

const int BIT_ORDER[8] = {B01000, B01100, B00100, B00110, B00010, B00011, B00001, B01001};
const int INIT_SPEED = 10;

void StepperMotorSmall::setup(int pins[4]) {
  _pin1 = pins[0];
  _pin2 = pins[1];
  _pin3 = pins[2];
  _pin4 = pins[3];
  pinMode(_pin1, OUTPUT);
  pinMode(_pin2, OUTPUT);
  pinMode(_pin3, OUTPUT);
  pinMode(_pin4, OUTPUT);
  _stp = 0;
  _stpCount = 0;
  _dir = HIGH;
  _speed = INIT_SPEED;
  _speedCount = 0;
}

void StepperMotorSmall::loop() {
  if (_stp == -1) {
    _rotate();
  } else {
    if (_stp <= _stpCount) return;
    _rotate();
  }
}

void StepperMotorSmall::rotate(int dir) {
  _stp = -1;
  _dir = dir;
}

void StepperMotorSmall::rotate(long stp, int dir) {
  _stp = stp * 91000;
  _dir = dir;
  _stpCount = 0;
}

void StepperMotorSmall::stop() {
  _stp = 0;
  _stpCount = 0;
  _speed = INIT_SPEED;
}

//void StepperMotorSmall::changeSpeed(int s) {
//  if (s < 1) s = 1;
//  if (s > 100) s = 100;
//  _speed = map(s, 1, 100, 4000, 1000);
//}
//
//void StepperMotorSmall::_rotate() {
//  if (_dir == HIGH) {
//    for (int i = 0; i < 8; i++) {
//      _digitalWrite(BIT_ORDER[i]);
//    }
//  } else {
//    for (int i = 7; i >= 0; i--) {
//      _digitalWrite(BIT_ORDER[i]);
//    }
//  }
//}
//
//void StepperMotorSmall::_digitalWrite(int bit4) {
//  digitalWrite(_pin1, bitRead(bit4, 0));
//  digitalWrite(_pin2, bitRead(bit4, 1));
//  digitalWrite(_pin3, bitRead(bit4, 2));
//  digitalWrite(_pin4, bitRead(bit4, 3));
//  delayMicroseconds(_speed);
//}

void StepperMotorSmall::changeSpeed(int s) {
  if (s < 1) s = 1;
  if (s > 100) s = 100;
  _speed = map(s, 1, 100, INIT_SPEED * 4, INIT_SPEED);
}

void StepperMotorSmall::_rotate() {
  if (_dir == HIGH) {
    if (_speedCount % _speed == 0) {
      int i = _speedCount / _speed;
      if (i == 7) _speedCount = 0;
      _digitalWrite(BIT_ORDER[i]);
    }
    _speedCount++;
    _stpCount++;
  } else {
    if (_speedCount % _speed == 0) {
      int i = _speedCount / _speed;
      if (i == 0) _speedCount = _speed * 8;
      _digitalWrite(BIT_ORDER[i]);
    }
    _speedCount--;
    _stpCount++;
  }
}

void StepperMotorSmall::_digitalWrite(int bit4) {
  digitalWrite(_pin1, bitRead(bit4, 0));
  digitalWrite(_pin2, bitRead(bit4, 1));
  digitalWrite(_pin3, bitRead(bit4, 2));
  digitalWrite(_pin4, bitRead(bit4, 3));
}
