#include "WarningLight.h"

const int OFF = 0, FLICKER = 1, FADE = 2;

void WarningLight::setup(int pin) {
  _mode = OFF;
  _pin = pin;
  _cnt = 0;
  _voltage = LOW;
  _interval = 0;
  pinMode(_pin, OUTPUT);
}

void WarningLight::loop() {
  switch (_mode) {
    case OFF:
      _cnt = 0;
      _voltage = LOW;
      digitalWrite(_pin, _voltage);
      break;
    case FLICKER:
      if (_cnt == 0) {
        _voltage = _voltage == LOW ? HIGH : LOW;
      }
      _cnt++;
      if (_cnt == _interval) {
        _cnt = 0;
      }
      digitalWrite(_pin, _voltage);
      break;
    case FADE:
      if (_cnt == 0) {
        _voltage = HIGH;
      } else if (_cnt == _interval) {
        _voltage = LOW;
      }
      if (_voltage == HIGH) _cnt++;
      else _cnt--;
      analogWrite(_pin, map(_cnt, 0, _interval, 0, 255));
      break;
  }
}

void WarningLight::off() {
  _mode = OFF;
}

void WarningLight::flicker(long interval) {
  _mode = FLICKER;
  _cnt = 0;
  _voltage = LOW;
  _interval = interval;
}

void WarningLight::fade(long interval) {
  _mode = FADE;
  _cnt = 0;
  _interval = interval / 5;
}
