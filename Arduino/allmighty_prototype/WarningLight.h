#ifndef WarningLight_h
#define WarningLight_h

#include "Arduino.h"

class WarningLight {
  public:
    void setup(int pin);
    void loop();
    void off();
    void flicker(long interval);
    void fade(long interval);

  private:
    int _mode;
    int _pin;
    long _cnt;
    int _voltage;
    long _interval;
};

#endif
