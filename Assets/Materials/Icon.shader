﻿Shader "Custom/Icon" {
    Properties{
        _MainTex("Albedo (RGB)", 2D) = "white" {}
        _Color("Color", Color) = (1,1,1,1)
    }
        SubShader{
            Tags{ "RenderType" = "Transparent" "Queue" = "Transparent" }

            CGPROGRAM

        #pragma surface surf Lambert noambient alpha:fade


        sampler2D _MainTex;
		half3 _Color;

        struct Input {
            float2 uv_MainTex;
            float3 viewDir;
        };

        void surf(Input IN, inout SurfaceOutput o) {
            float4 c = tex2D(_MainTex, IN.uv_MainTex);

            //o.Emission = float3(0, 0.8f, 1);
			o.Emission = _Color;
            //o.Emission = _Color;
            float3 rim = dot(o.Normal, IN.viewDir);
            rim = pow(1 - rim, 3) + 0.2f;
            o.Alpha = rim;
        }

        ENDCG
    }
        FallBack "Diffuse"
}