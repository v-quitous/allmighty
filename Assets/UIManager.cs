﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    GameObject lightUIPrefab;

    // Start is called before the first frame update
    void Start()
    {
        lightUIPrefab = GameObject.Find("lightUIPrefab");
        lightUIPrefab.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            lightUIPrefab.SetActive(true);
        }
    }
}
