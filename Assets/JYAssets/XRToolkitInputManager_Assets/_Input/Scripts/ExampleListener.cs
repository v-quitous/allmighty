﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class ExampleListener : MonoBehaviour
{
    public ButtonHandler primaryAxisClickHandler = null;

    public bool isLPBDown= false;

    public void OnEnable()
    {
        primaryAxisClickHandler.OnButtonDown += PrintPrimaryButtonDown;
        primaryAxisClickHandler.OnButtonUp += PrintPrimaryButtonUp;
    }

    public void OnDisable()
    {
        primaryAxisClickHandler.OnButtonDown -= PrintPrimaryButtonDown;
        primaryAxisClickHandler.OnButtonUp -= PrintPrimaryButtonUp;

    }

    private void PrintPrimaryButtonDown(XRController controller)
    {
        isLPBDown = true;
        print("isLPBDown:"+ isLPBDown);
        GameObject.Find("NewCampus").GetComponent<ObjectsFloat>().enabled = false;
        GameObject.Find("NewCampus").GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationZ;
        GameObject.Find("NewCampus").GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition;
        GameObject.Find("ALS").transform.parent = null;
        //GameObject.Find("NewCampus").GetComponent<BoxCollider>().enabled = false;
        GameObject.Find("ALS").GetComponent<BoxCollider>().enabled = true;
        
    }

    private void PrintPrimaryButtonUp(XRController controller)
    {
        isLPBDown = false;
        print("isLPBDown:"+ isLPBDown);
        //GameObject.Find("NewCampus").GetComponent<ObjectsFloat>().enabled = true;
        //GameObject.Find("NewCampus").GetComponent<BoxCollider>().enabled = true;
        //GameObject.Find("ALS").GetComponent<BoxCollider>().enabled = false;
    }

    private void PrintPrimaryAxis(XRController controller, Vector2 value)
    {

    }

    private void PrintTrigger(XRController controller, float value)
    {

    }
}
