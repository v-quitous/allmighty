﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JY_UIAdd : MonoBehaviour
{
    GameObject lightUIPrefab;
    GameObject CCTVUIPrefab;

    // Start is called before the first frame update
    void Start()
    {
        lightUIPrefab = GameObject.Find("lightUIPrefab");
        lightUIPrefab.SetActive(false);
        CCTVUIPrefab = GameObject.Find("CCTVUIPrefab");
        CCTVUIPrefab.SetActive(false);
    }



    // Update is called once per frame
    void Update()
    {

        if(Input.GetKeyDown(KeyCode.Alpha3))
        {
            lightUIPrefab.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            lightUIPrefab.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
           CCTVUIPrefab.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            CCTVUIPrefab.SetActive(false);
        }
    }
}
