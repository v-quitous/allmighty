﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RomscaleEnter : MonoBehaviour
{
    GameObject changeViewPannel;
    Image darkBlue;
    bool isFadedIn;

    // Update is called once per frame
    void Update()
    {
        StartCoroutine("FadeIn");
    }
    IEnumerator FadeIn()
    {
        Color color = darkBlue.color;
        for (int i = 100; i > 0; i--)
        {
            color.a -= Time.deltaTime * 0.01f;
            darkBlue.color = color;
            if (darkBlue.color.a <= 0)

            {
                isFadedIn = true;
            }
            yield return null;
        }
    }
}
