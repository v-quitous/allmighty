﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteZoom : MonoBehaviour
{
    Transform cam, endPoint;
    GameObject blinder;
    GameObject tornado;
    GameObject spaceship;
    Vector3 dir;
    public float speed = 50f;
    public float finishLine = 5f;
    private float waveLength = 1f;
    private float amplitude = 1f;
    public float blineDistance = 200f;
    public float ParticleDistance;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main.transform;
        endPoint = GameObject.Find("EndPoint").transform;
        cam.forward = -endPoint.forward;
        cam.LookAt(endPoint);

        waveLength = Random.Range(50f, 100f);
        amplitude = Random.Range(5f, 7f);

        blinder = GameObject.Find("Blinder");
        blinder.transform.LookAt(endPoint);

        tornado = GameObject.Find("Tornado");

        spaceship = gameObject.transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(CameraMove());
        }
    }

    IEnumerator CameraMove()
    {
        while (true)
        {
            dir = endPoint.position - cam.position;
            Vector3 upDownWave = cam.up * Mathf.Sin(Time.time * waveLength) * amplitude;
            Vector3 leftRightWave = cam.right * Mathf.Sin(Time.time * waveLength) * amplitude;

            cam.position += upDownWave + leftRightWave + dir.normalized * speed * Time.time;
            spaceship.transform.position += (upDownWave + leftRightWave) * Time.deltaTime;
            cam.LookAt(endPoint);

            float distance = (endPoint.transform.position - cam.transform.position).magnitude;
            if (distance < finishLine)
            {
                transform.GetChild(0).gameObject.SetActive(false);
                blinder.GetComponent<MeshRenderer>().material.color = new Color(0, 0, 0, 255);
                break;
            }
            yield return null;
        }
    }
}
