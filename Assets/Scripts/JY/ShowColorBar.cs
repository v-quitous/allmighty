﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowColorBar : MonoBehaviour, IEventController
{
    GameObject colorBar;
    // Start is called before the first frame update
    void Start()
    {
        colorBar= GameObject.Find("BG_Glass (6)");
        colorBar.SetActive(false);
    }

    public void HoverEvent()
    {
        
    }

    public void SelectEvent()
    {
        colorBar.SetActive(true);
    }
    public void StopHoverEvent()
    {
       
    }
    public void StopSelectEvent()
    {
       
    }
}

