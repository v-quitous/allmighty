﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmerOneTouch : MonoBehaviour, IEventController
{
    bool isSelect, isHovering;

    // Update is called once per frame
    void Update()
    {
        if(isSelect==true)
        {
            EmerOperation();
        }
    }
    private void EmerOperation()
    {
        Ino_Manager.Emergency_OneTouch();
    }

    public void HoverEvent()
    {
        isHovering = true;
    }
    public void SelectEvent()
    {
        isSelect = true;
    }
    public void StopHoverEvent()
    {
        isHovering = false;
    }
    public void StopSelectEvent()
    {
        isSelect = false;
    }
}
