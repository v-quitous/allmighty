﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorStatus : MonoBehaviour, IEventController
{
    bool isSelected = false;
    public GameObject doorStatus;
    public int status;

    public void HoverEvent()
    {
        
    }

    public void SelectEvent()
    {
        doorStatus.SetActive(true);
        isSelected = true;
        if(status == 1)
        {
            SelectColorChange.ToggleLight();
        }
        else if (status == 2)
        {
            SelectColorChange.ToggleDoor();
        }
        else if (status == 3)
        {
            SelectColorChange.ToggleWindow();
        }
        else if (status == 4)
        {
            SelectColorChange.TogglePG();
        }
    }

    public void StopHoverEvent()
    {
    }

    public void StopSelectEvent()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        doorStatus.SetActive(false);
    }
}
