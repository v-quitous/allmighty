﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseUI : MonoBehaviour, IEventController
{
    public GameObject toDestroy;

    public void HoverEvent()
    {

    }

    public void SelectEvent()
    {
        PanelManager.DestroySelf();
    }

    public void StopHoverEvent()
    {

    }

    public void StopSelectEvent()
    {

    }
}
