﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmptyEvent : MonoBehaviour, IEventController
{
    public void HoverEvent()
    {
        return;
    }
    public void SelectEvent()
    {
        return;
    }
    public void StopHoverEvent()
    {
        return;
    }
    public void StopSelectEvent()
    {
        return;
    }
}
