﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JY_Ray : MonoBehaviour
{
    GameObject rightHand;
    LineRenderer lr;

    public static JY_Ray Instance;
    private void Awake()
    {
        if (!Instance)
            Instance = this;
    }

    private void Start()
    {
        rightHand = GameObject.Find("RightHandAnchor");
        lr = GetComponent<LineRenderer>();
    }

    void Update()
    {
        GetRHandRay();
        DrawRayLine();
        CheckInteraction();
    }


    Transform objectHit;
    Vector3 hitPosition;
    private void GetRHandRay()
    {
        RaycastHit hit;
        Ray ray = new Ray(rightHand.transform.position, rightHand.transform.forward);
        if (Physics.Raycast(ray, out hit))
        {
            objectHit = hit.transform;
            hitPosition = hit.point;
        }
        else
        {
            objectHit = null;
        }
    }

    Vector3 rayPos = Vector3.zero;
    public Vector3 GetMouseWorldPos()
    {
        GetRHandRay();
        if (objectHit)
        {
            rayPos = hitPosition;
        }
        return rayPos;
    }

    private void DrawRayLine()
    {
        lr.positionCount = 2;
        if (rightHand)
        {
            lr.SetPosition(0, rightHand.transform.position);
            lr.SetPosition(1, hitPosition);
        }
    }

    JY_ColorWheelControl jycw;
    private void CheckInteraction()
    {
        if (OVRInput.GetDown(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.RTouch))
        {
            IWorldUI iwui = objectHit.gameObject.GetComponent<IWorldUI>();
            if (iwui != null)
                iwui.OnClick();
            jycw = objectHit.gameObject.GetComponent<JY_ColorWheelControl>();
            if (jycw)
            {
                jycw.ButtonDown();
            }
        }
        if(jycw)
        {
            if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.RTouch) != 0)
            {
                jycw.ButtonStay();
            }
            else
            {
                jycw.ButtonUp();
                jycw = null;
            }
        }
    }
}
