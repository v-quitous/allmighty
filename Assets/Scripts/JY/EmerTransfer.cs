﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmerTransfer : MonoBehaviour, IEventController
{
    GameObject emercanvas;

    // Start is called before the first frame update
    void Start()
    {
        emercanvas = GameObject.Find("EmerCanvas");
    }

    public void HoverEvent()
    {
    }

    public void SelectEvent()
    {
        emercanvas.SetActive(false);

    }

    public void StopHoverEvent()
    {
    }

    public void StopSelectEvent()
    {
    }
}
