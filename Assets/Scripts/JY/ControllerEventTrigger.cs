﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//중지를 누르면 Hover(=Ray쏘고 있는 상태)
//중지 누른 상태에서 검지도 누르면 Select

public class ControllerEventTrigger : MonoBehaviour
{
    GameObject lHand, rHand;

    Ray lRay, rRay;
    public RaycastHit lHitInfo, rHitInfo;
    IEventController eventController;
    
    bool isLHandHovering, isRHandHovering, isLHandSelect, isRHandSelect;
    bool isLHandRayBeenCasted, isRHandRayBeenCassted;

    LineRenderer llr, rlr;

    // Start is called before the first frame update
    void Start()
    {
        lHand = GameObject.Find("LeftHandAnchor");
        rHand = GameObject.Find("RightHandAnchor");
        llr = lHand.GetComponent<LineRenderer>();
        llr.enabled = false;
        rlr = rHand.GetComponent<LineRenderer>();
        rlr.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        lHandRay();
        lHandSelect();
        rHandRay();
        rHandSelect();
    }

    private void DrawLRayLine()
    {
        llr.enabled = true;
        llr.positionCount = 2;
            llr.SetPosition(0, lHand.transform.position);
            llr.SetPosition(1, lHitInfo.point);
    }
    private void DrawRRayLine()
    {
        rlr.enabled = true;
        rlr.positionCount = 2;
        rlr.SetPosition(0, rHand.transform.position);
        rlr.SetPosition(1, rHitInfo.point);
    }

    //중지를 누르면 레이 쏨
    private void lHandRay()
    {
        if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.LTouch) != 0)
        {
            lRay = new Ray(lHand.transform.position, lHand.transform.forward);
            DrawLRayLine();
            if (Physics.Raycast(lRay, out lHitInfo))
            {
                eventController = lHitInfo.transform.gameObject.GetComponent<IEventController>();
                print("left ray eventController:" + eventController);
                print("left ray object:" + lHitInfo.transform.gameObject.name);
                if (eventController != null)
                {
                    isLHandHovering = true;
                    eventController.HoverEvent();
                    print("left HoverEvent");
                }
            }
        }
        if (eventController!= null&& OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.LTouch))
        {
            llr.enabled = false;
            isLHandHovering = false;
            eventController.StopHoverEvent();
            print("left StopHoverEvent");
            eventController = null;
        }
    }

    //레이를 쏘고 있는 상태에서 검지를 누르면 SelectEvent 실행
    private void lHandSelect()
    {
        if (isLHandHovering == true && OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.LTouch) != 0)
        {
            if (eventController != null)
            {
                isLHandSelect = true;
                eventController.SelectEvent();
                print("left SelectEvent");
            }
        }
        if(isLHandHovering== true&& OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.LTouch))
        {
            isLHandSelect = false;
            eventController.StopSelectEvent();
            print("left StopSelectEvent");
            eventController.StopHoverEvent();
            eventController = null;
        }
    }

    private void rHandRay()
    {
        if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.RTouch) != 0)

        {
            rRay = new Ray(rHand.transform.position, rHand.transform.forward);
            DrawRRayLine();
            if (Physics.Raycast(rRay, out rHitInfo))
            {
                eventController = rHitInfo.transform.gameObject.GetComponent<IEventController>();
                print("right ray eventController:" + eventController);
                print("right ray object:" + rHitInfo.transform.gameObject.name);
                if (eventController != null)
                {
                    isRHandHovering = true;
                    eventController.HoverEvent();
                    print("right HoverEvent");
                }
            }
        }
        if (eventController!= null && OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.RTouch))
        {
            rlr.enabled = false;
            isRHandHovering = false;
            eventController.StopHoverEvent();
            print("right StopHoverEvent");
            eventController = null;
        }
    }

    private void rHandSelect()
    {
        if (isRHandHovering == true && OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch) != 0)
        {
            if (eventController != null)
            {
                isRHandSelect = true;
                eventController.SelectEvent();
                print("right SelectEvent");
            }
        }
        if (isRHandHovering==true&& OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch))
        {
            isRHandSelect = false;
            eventController.StopSelectEvent();
            eventController.StopHoverEvent();
            print("right StopSelectEvent");
            eventController = null;
        }
    }
}
