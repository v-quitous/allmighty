﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//방을 클릭하면 방 안으로 들어감
//필요속성: 방, 검지, 카메라, 캔버스(파란색), 룸스케일 씬

public class InfiniteZoomforRooms : MonoBehaviour
{
    GameObject room203;
    GameObject camera;
    Collider indexCollider;
    GameObject changeViewPannel;
    Image darkBlue;
    bool isFadedOut;
    bool isZoomStart;
    public float zoomSpeed;

    // Start is called before the first frame update
    void Start()
    {
        room203 = GameObject.Find("Room203");
        camera = GameObject.Find("Main Camera");
        changeViewPannel = GameObject.Find("CanvasforChangeView");
    }

    // Update is called once per frame
    void Update()
    {
        ZoomInStart();
        if (isZoomStart == true)
        {
            //2. 카메라가 해당 위치로 빠르게 이동
            camera.transform.position = Vector3.Lerp(camera.transform.position, room203.transform.position, zoomSpeed*Time.deltaTime);
            if (Vector3.Distance(camera.transform.position, room203.transform.position) < 0.1f)
            {
                camera.transform.position = room203.transform.position;
                changeViewPannel.SetActive(true);
                StartCoroutine("FadeOut");
                if (isFadedOut)
                {
                    darkBlue.enabled = false;
                    SceneManager.LoadScene("RoomScaleView");
                }
                isZoomStart = false;
            }
        }
    }

    private void ZoomInStart()
    {
        if(isZoomStart==false)
        {
            isZoomStart = true;
        }
    }

    //1. 방 위치와 검지가 충돌
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == indexCollider)
        {
            ZoomInStart();
        }
    }

    //3. 화면이 짙은 파란색으로 변함
    IEnumerator FadeOut()
    {
        Color color = darkBlue.color;
        for (int i = 0; i <100; i++)
        {
            color.a -= Time.deltaTime * 0.005f;
            darkBlue.color = color;
            if (darkBlue.color.a >= 100)
            {
                isFadedOut = true;
            }
            yield return null;
        }
    }
}

