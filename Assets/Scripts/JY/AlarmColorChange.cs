﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlarmColorChange : MonoBehaviour
{
    public Material matAlarm;
    Color origColor;
    Color targetColor;

    public static AlarmColorChange Instance;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        origColor = new Color(1, 178 / 255f, 0, 1);
        targetColor = new Color(1, 0, 0, 1);
        matAlarm.color = origColor;
        ToggleAlarm();
    }

    float lerpSpeed = 4.5f;
    bool toOrig = false;
    bool toTarg = false;
    // Update is called once per frame
    void Update()
    {
        if (caseAlarm)
            ChangeMatColor(matAlarm);
    }

    bool caseAlarm = false;
    bool filterOn = false;
    private void ChangeMatColor(Material mat)
    {
        if (filterOn)
        {
            if (EJ_FloorTools.CheckApprox(mat.color, origColor))
            {
                print("is orig color");
                toOrig = false;
                toTarg = true;
            }
            if (EJ_FloorTools.CheckApprox(mat.color, targetColor))
            {
                print("is target color");
                toOrig = true;
                toTarg = false;
            }
            if (toOrig)
            {
                EJ_FloorTools.ColorLerpTo(mat, origColor, lerpSpeed);

            }
            if (toTarg)
            {
                EJ_FloorTools.ColorLerpTo(mat, targetColor, lerpSpeed);

            }
        }
        else
        {
            EJ_FloorTools.ColorLerpTo(mat, origColor, lerpSpeed);
        }
    }

    public static void ToggleAlarm()
    {
        Instance.caseAlarm = !Instance.caseAlarm;
        Instance.filterOn = !Instance.filterOn;
    }
}
