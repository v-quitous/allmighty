﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//select하면 기기별 제어 아이콘 모음을 띄움
//다시 selcet하면 기기별 제어 아이콘 모음을 없앰
//필요속성: 기기별 제어 모음 아이콘

public class ShowDeviceMenu : MonoBehaviour, IEventController
{
    public bool isHovering, isSelect, isUnSelect;
    bool isDevicesMenuOpen;
    GameObject devicesMenu;

    // Start is called before the first frame update
    void Start()
    {
        devicesMenu = GameObject.Find("DeviceIcons");
        devicesMenu.SetActive(false);
        this.GetComponent<ObjectsFloat>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isDevicesMenuOpen == true)
        {
            devicesMenu.SetActive(true);
            isSelect = false;
        }
        if(devicesMenu.activeSelf== true&& isSelect== true)
        {
            CloseDevicesMenu();
        }
        if(isDevicesMenuOpen == false)
        {
            devicesMenu.SetActive(false);
        }
    }

    private void OpenDevicesMenu()
    {
        isDevicesMenuOpen = true;
    }

    private void CloseDevicesMenu()
    {
        isDevicesMenuOpen = false;
    }

    public void HoverEvent()
    {
        isHovering = true;
        this.transform.parent = null;
        this.GetComponent<ObjectsFloat>().enabled = true;
    }
    public void SelectEvent()
    {
        isSelect = true;
        OpenDevicesMenu();
        
    }
    public void StopHoverEvent()
    {
        isHovering = false;
        this.transform.parent = GameObject.Find("Menu").transform;
        this.GetComponent<ObjectsFloat>().enabled = false;
    }
    public void StopSelectEvent()
    {
        isSelect = false;
    }
}
