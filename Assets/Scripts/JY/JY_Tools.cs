﻿using UnityEngine;

public static class JY_Tools
{
    public static Vector3 GetHitPosWorld()
    {
        return JY_Ray.Instance.GetMouseWorldPos();
    }
    public static Vector2 GetHitPos()
    {
        Vector3 v = GetHitPosWorld();
        return new Vector2(v.x, v.y);
    }
}