﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyPanel : MonoBehaviour, IEventController
{
    public GameObject setfalse; 
    public void HoverEvent()
    {
    }

    public void SelectEvent()
    {
        setfalse.SetActive(false);
    }

    public void StopHoverEvent()
    {
    }

    public void StopSelectEvent()
    {
    }
}
