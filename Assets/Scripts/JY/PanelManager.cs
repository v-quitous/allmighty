﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelManager : MonoBehaviour
{
    public static PanelManager Instance;

    private void Awake()
    {
        if(Instance== null)
        {
            Instance = this;
        }
    }
    public static void DestroySelf()
    {
        Destroy(Instance.gameObject);
    }

}
