﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//특정한 방이 Select 되었을 때 그 방으로 워프
//Warp가 끝나면 씬을 전환
//필요속성: 워프 시작점, 워프 도착점, 걸리는 시간, 로드할 씬

public class Warp : MonoBehaviour, IEventController
{
    Vector3 startPoint;
    Transform endTransform;
    float currentTime;
    public float warpTime;
    bool isWarp;
    GameObject camera;

    // Start is called before the first frame update
    void Start()
    {
        camera = GameObject.Find("OVRCameraRig");
    }

    public void HoverEvent()
    {
    }
    public void SelectEvent()
    {
        isWarp = true;
        StartCoroutine(WarptoPoint());
    }

    public void StopHoverEvent()
    {

    }
    public void StopSelectEvent()
    {

    }
    public IEnumerator WarptoPoint()
    {
        startPoint = camera.transform.position;
        endTransform = this.transform;
        if (endTransform.gameObject.tag == "point")
        {
            currentTime = 0;
            while (currentTime / warpTime < 1)
            {
                currentTime += Time.deltaTime;
                camera.transform.position = Vector3.Lerp(startPoint, endTransform.position, currentTime / warpTime);
                yield return null;
            }
            camera.transform.position = endTransform.position;
            SceneManager.LoadScene("201124_0_JY_OculusLightControl");
        }
    }
}
