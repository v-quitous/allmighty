﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//204호를 셀렉하면 204호 씬으로 이동

public class RoomScaletoRoomScale : MonoBehaviour, IEventController
{
    bool isHovering, isSelect;
    
    // Update is called once per frame
    void Update()
    {
        if(isHovering== true)
        {
            this.GetComponent<ObjectsFloat>().frequency= 1.7f;
            this.GetComponent<ObjectsFloat>().amplitude = 0.001f;

        }
        if (isSelect==true)
        {
            SceneManager.LoadScene("JY_204Scene");
        }
    }

    public void HoverEvent()
    {
        isHovering = true;
    }
    public void SelectEvent()
    {
        isSelect = true;
    }
    public void StopHoverEvent()
    {
        isHovering = false;
    }
    public void StopSelectEvent()
    {
        isSelect = false;
    }
}
