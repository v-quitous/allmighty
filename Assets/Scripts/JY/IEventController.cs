﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEventController
{
    void HoverEvent();
    void SelectEvent();
    void StopHoverEvent();
    void StopSelectEvent();
}
