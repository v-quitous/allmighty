﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPressed : MonoBehaviour, IEventController
{
    Vector3 originScale;
    public float speed=1;
    bool isSelect = false;
    Vector3 smallScale;

    // Start is called before the first frame update
    void Start()
    {
        originScale = this.transform.localScale;
        smallScale = originScale * 0.5f;
    }

    bool makeSmall = true;
    bool backToOrigin = false;
    // Update is called once per frame
    void Update()
    {
        if (isSelect==true)
        {
            ButtonPressEffect();
        }
    }

    void IEventController.HoverEvent()
    {
    }

    void IEventController.SelectEvent()
    {
        isSelect = true;
    }

    void IEventController.StopHoverEvent()
    {
    }

    void IEventController.StopSelectEvent()
    {
    }

    private void ButtonPressEffect()
    {
        if (makeSmall)
        {
            this.transform.localScale = Vector3.Lerp(this.transform.localScale, smallScale, speed * Time.deltaTime);
            if(Vector3.Distance(this.transform.localScale, smallScale) < 0.01f)
            {
                this.transform.localScale = smallScale;
                makeSmall = false;
                backToOrigin = true;
            }
        }
        if(backToOrigin)
        {
            this.transform.localScale = Vector3.Lerp(this.transform.localScale, originScale, speed * Time.deltaTime);
            if (Vector3.Distance(this.transform.localScale, originScale) < 0.01f)
            {
                this.transform.localScale = originScale;
                backToOrigin = false;
            }
        }

    }
}
