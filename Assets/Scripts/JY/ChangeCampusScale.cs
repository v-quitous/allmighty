﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// 3D 캠퍼스맵을 양손으로 잡아서 늘리고 싶다. 그립 상태에서 양손 위치에 비례한다.
// 맵에서 특정한 단지를 선택하면 빠른 참조를 해당 건물에 맞게 바꾸고 싶다.
// 필요속성: 손 위치, 캠퍼스맵, 농대 단지, 농대 빠른참조


public class ChangeCampusScale : MonoBehaviour
{
    GameObject lHand, rHand;

    GameObject campusBody, ALS;// building200;
    Vector3 originScale;
    bool isCampusGrip;
    float scaleTimes;

    //Ray lRay, rRay;
    //RaycastHit lHitInfo, rHitInfo;

    //Quaternion originRHandRot, newRHandRot, campusRot;
    //float degree;


    // Start is called before the first frame update
    void Start()
    {
        lHand = GameObject.Find("Hand_Left");
        rHand = GameObject.Find("Hand_Right");
        campusBody = GameObject.Find("NewCampus");
        //ALS = GameObject.Find("ALS_withNumber");
        //building200 = GameObject.Find("AgriBuilding");

        originScale = campusBody.transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        // 왼손과 오른손이 모두 그립 상태라면--> 캠퍼스를 그립했다는 걸 어떻게 표현하지?
        if(lHand.GetComponent<HandAnimator>().isGrip==true && rHand.GetComponent<HandAnimator>().isGrip == true)
        {
            isCampusGrip = true;
            //print("컨트롤러 사이 거리:"+ Vector3.Distance(lHand.transform.position, rHand.transform.position));
        }
        if (lHand.GetComponent<HandAnimator>().isGrip == false || rHand.GetComponent<HandAnimator>().isGrip == false)
        {
            isCampusGrip = false;
            //print("컨트롤러 사이 거리:"+ Vector3.Distance(lHand.transform.position, rHand.transform.position));
        }
        //else
        //{
        //    isCampusGrip = false;
        //}

        if (isCampusGrip== true && isCampusGrip!= false)
        {
            //양손 사이 거리를 1이라고 하고, 닿은 오브젝트의 트랜스폼 스케일을 거리에 비례해 조절한다
            //campusBody.transform.localScale *= (Mathf.Abs(Vector3.Distance(lHand.transform.position, rHand.transform.position)) * 5 / originScale.y);
            scaleTimes= Vector3.Distance(lHand.transform.position, rHand.transform.position) * 2.22f; //컨트롤러 사이 거리: 0.08(나란히 내려놨을 때)~0.45(캠퍼스 크기만큼 벌렸을 때)-> 0.45.를 1로 보정하려면 *2.22f
            Mathf.Clamp(scaleTimes, 0.4f, 1.5f);   
            campusBody.transform.localScale = originScale* scaleTimes;
            print("크기 조절 되고있음");
        }

        //// 캠퍼스 회전
        ////왼손으로 캠퍼스에 레이를 쏘고 있는 상태에서
        ////오른손으로 A버튼을 누른 채 오른손 각도를 바꾸면
        ////바뀌는 오른손 각도에 따라 캠퍼스가 회전한다

        ////A버튼 누르는 순간 각도를 저장한다
        ////if (Physics.Raycast(lRay, out lHitInfo) && OVRInput.GetDown(OVRInput.Button.One))
        //if (Physics.Raycast(lRay, out lHitInfo) && OVRInput.GetDown(OVRInput.Button.PrimaryThumbstick))
        //{
        //    originRHandRot = rHand.transform.rotation;
        //}
        ////A버튼 누르고 있는 동안 각도 변화를 받아 캠퍼스를 회전시킨다.
        ////if (Physics.Raycast(lRay, out lHitInfo) && OVRInput.Get(OVRInput.Button.One))
        //if (Physics.Raycast(lRay, out lHitInfo) && OVRInput.Get(OVRInput.Button.PrimaryThumbstick))
        //{
        //    newRHandRot = rHand.transform.rotation;
        //    degree = newRHandRot.y - originRHandRot.y;
        //    campusRot = campusBody.transform.localRotation;
        //    campusRot.y = degree;
        //    campusBody.transform.localRotation *= campusRot;

        //}

    }
}
