﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectColorChange : MonoBehaviour
{
    public Material matParkinggate;
    public Material matLight;
    public Material matDoor;
    public Material matWindow;
    Color origColor;
    Color targetColor;

    public static SelectColorChange Instance;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        origColor = new Color(0, 217 / 255f, 1, 1);
        targetColor = new Color(1, 178 / 255f, 0, 1);
        matParkinggate.color = origColor;
        matLight.color = origColor;
        matDoor.color = origColor;
        matWindow.color = origColor;
    }

    float lerpSpeed = 4.5f;
    bool toOrig = false;
    bool toTarg = false;
    // Update is called once per frame
    void Update()
    {
        if (casePG)
            ChangeMatColor(matParkinggate);
        if(caseLight)
            ChangeMatColor(matLight);
        if(caseDoor)
            ChangeMatColor(matDoor);
        if (caseWindow)
            ChangeMatColor(matWindow);
    }

    bool casePG = false;
    bool caseLight = false;
    bool caseDoor = false;
    bool caseWindow = false;
    bool filterOn = false;
    private void ChangeMatColor(Material mat)
    {
        if (filterOn)
        {
            if (EJ_FloorTools.CheckApprox(mat.color, origColor))
            {
                print("is orig color");
                toOrig = false;
                toTarg = true;
            }
            if (EJ_FloorTools.CheckApprox(mat.color, targetColor))
            {
                print("is target color");
                toOrig = true;
                toTarg = false;
            }
            if (toOrig)
            {
                EJ_FloorTools.ColorLerpTo(mat, origColor, lerpSpeed);

            }
            if (toTarg)
            {
                EJ_FloorTools.ColorLerpTo(mat, targetColor, lerpSpeed);

            }
        }
        else
        {
            EJ_FloorTools.ColorLerpTo(mat, origColor, lerpSpeed);
        }
    }

    public static void TogglePG()
    {
        Instance.casePG = !Instance.casePG;
        Instance.filterOn = !Instance.filterOn;
    }
    public static void ToggleLight()
    {
        Instance.caseLight = !Instance.caseLight;
        Instance.filterOn = !Instance.filterOn;
    }
    public static void ToggleDoor()
    {
        Instance.caseDoor = !Instance.caseDoor;
        Instance.filterOn = !Instance.filterOn;
    }
    public static void ToggleWindow()
    {
        Instance.caseWindow = !Instance.caseWindow;
        Instance.filterOn = !Instance.filterOn;
    }
}
