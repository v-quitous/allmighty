﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//시작하면 Rooms가 촤라락 나온다
//Buildings 두꺼운 선 부분 안쪽에 단과대명이 나오고 Rooms 점 부분에는 동이 나온다

//농대를 클릭하면 캠퍼스맵 사라지고 농대 단지가 커지면서 나온다
// Rooms 없어지고 단과대명 없어진다
// Rooms 촤라락 뜬다. Buildings 두꺼운 선 부분 안쪽에 동명이 나오고 Rooms 점 부분에는 호실이 나온다

//필요속성: Rooms, Buildings, Campus, ALS, 단과대명, 동명, 호실, 촤라락 속도

public class CampusFastLookUP : MonoBehaviour
{
    GameObject roomsCircle;
    GameObject roomsCircle1;
    Image roomsCircleImg;
    Image roomsCircle1Img;
    GameObject buildingsCircle;
    GameObject campusBody;
    GameObject ALS;
    GameObject colleges;
    GameObject collegeBuildingNums;
    GameObject ALSBuildingNums;
    GameObject roomNums;
    GameObject selectedALS;
    GameObject building200;

    Transform selectPosition;

    public float spreadSpeed = 7;
    float currentTime;
    bool fillZero;

    // Start is called before the first frame update
    void Start()
    {
        roomsCircle = GameObject.Find("Rooms");
        roomsCircleImg = roomsCircle.GetComponent<UnityEngine.UI.Image>();
        roomsCircleImg.fillAmount = 0;

        roomsCircle1 = GameObject.Find("Rooms (1)");
        roomsCircle1Img = roomsCircle1.GetComponent<UnityEngine.UI.Image>();
        roomsCircle1Img.fillAmount = 0;

        buildingsCircle = GameObject.Find("Buildings");

        ALS = GameObject.Find("ALS");

        colleges = GameObject.Find("CollegeNames");
        collegeBuildingNums = GameObject.Find("CollegeBuildNums");
        collegeBuildingNums.SetActive(false);
        ALSBuildingNums = GameObject.Find("ALSBuildingNums");
        ALSBuildingNums.SetActive(false);
        roomNums = GameObject.Find("ALSRoomNums");
        roomNums.SetActive(false);
    }


    // Update is called once per frame
    void Update()
    {
        currentTime = 0;
        currentTime += Time.deltaTime;
        roomsCircle.GetComponent<UnityEngine.UI.Image>().fillAmount += Time.deltaTime * 1.3f;
        if (roomsCircle.GetComponent<UnityEngine.UI.Image>().fillAmount == 1)
        {
            collegeBuildingNums.SetActive(true);
        }

        //농대 선택됐을 떄
        if (ALS.GetComponent<SelectMove>().selectedtoEnlarge == true)
        {
            //단과대, 건물번호 사라지고
            colleges.SetActive(false);
            roomsCircle.GetComponent<UnityEngine.UI.Image>().fillAmount = 0f;
            collegeBuildingNums.SetActive(false);
            fillZero = true;
        }
    }
    private void LateUpdate()
    {
        if (fillZero == true)
        {
            //농대 동번호, 호실 번호 촤르르 등장
            ALSBuildingNums.SetActive(true);

            roomsCircle1.GetComponent<UnityEngine.UI.Image>().fillAmount += Time.deltaTime * 1.3f;
            if (roomsCircle1.GetComponent<UnityEngine.UI.Image>().fillAmount == 1)
            {
                roomNums.SetActive(true);
            }
        }
    }
}

