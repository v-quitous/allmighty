﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewChangeCampusScale : MonoBehaviour, IEventController
{
    GameObject lHand, rHand;
    GameObject campus, ALS, IconsandCampus;
    public Vector3 originScale, campusIconsOriginScale;
    Vector3 changedScale, changedCIScale;
    bool isCampusLocked;
    float scaleTimes;
    bool isHovering, isSelect;
    bool isScaleChanged, isRotationChanged;
    Ray lRay, rRay;
    RaycastHit lHitInfo, rHitInfo;
    Vector3 prevCampusRot; 
    
    // Start is called before the first frame update
    void Start()
    {
        lHand = GameObject.Find("LeftHandAnchor");
        rHand = GameObject.Find("RightHandAnchor");
        IconsandCampus = GameObject.Find("IconsandCampus");

        originScale = this.transform.localScale;
        prevCampusRot = this.transform.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        if(isSelect== true)
        {
            ChangeCampusScale();
            isScaleChanged = true;
        }
        if(isScaleChanged==true&& isSelect==false)
        {
            this.transform.localScale = changedScale;
        }

        if (isHovering==true && isSelect==false)
        {
            NewCampusRotate();
            isRotationChanged = true;
        }
    }

    //캠퍼스맵을 오른손으로 돌림- 오른손의 이동 속도와 방향에 비례해 캠퍼스를 y축 중심으로 회전
    //필요 속성: 오른손 이전 위치, (오른손 현재 위치), 오른손 회전력(회전 속도), 캠퍼스 각도

    Quaternion prevRHandRot;
    Rigidbody rb;
    float angle;
    Vector3 axis;
    Quaternion deltaRotation;

    Quaternion newCampusRotQ;
    GameObject campusIcons;
    private void NewCampusRotate()
    {
        deltaRotation = rHand.transform.rotation * Quaternion.Inverse(prevRHandRot);
        deltaRotation.ToAngleAxis(out angle, out axis);
        rb = rHand.GetComponent<Rigidbody>();
        rb.angularVelocity = (angle * axis) / Time.deltaTime;
        print("angle:" + angle + ", axis:" + axis);
        float clampedYVel = Mathf.Clamp(rb.angularVelocity.y, -1000, 1000);
        print("각속도:" + rb.angularVelocity.y + "," + clampedYVel);
        Quaternion newCampusRot= Quaternion.AngleAxis(clampedYVel*Time.deltaTime, this.transform.up);
        this.transform.rotation = Quaternion.Inverse(newCampusRot) * this.transform.rotation;
        campusIcons = GameObject.Find("CampusIcons");
        campusIcons.transform.rotation = Quaternion.Inverse(newCampusRot) * this.transform.rotation;
        prevRHandRot = rHand.transform.rotation;
    }

    //캠퍼스맵을 양손으로 잡아서 늘림- 레이를 쏜 상태에서 양손 위치에 비례한다.
    private void ChangeCampusScale()
    {
        scaleTimes = Vector3.Distance(lHand.transform.position, rHand.transform.position) * 2.22f; //컨트롤러 사이 거리: 0.08(나란히 내려놨을 때)~0.45(캠퍼스 크기만큼 벌렸을 때)-> 0.45.를 1로 보정하려면 *2.22f
        Mathf.Clamp(scaleTimes, 0.4f, 1.5f);
        this.transform.localScale = originScale * scaleTimes;
        campusIcons = GameObject.Find("CampusIcons");
        campusIcons.transform.localScale = campusIconsOriginScale * scaleTimes;
        //크기 변한 상태를 저장하고 컨트롤러에서 손을 떼면 그 상태를 유지
        changedScale = this.transform.localScale;
    }

    public void HoverEvent()
    {
        isHovering = true;
    }
    public void SelectEvent()
    {
        isSelect = true;
    }
    public void StopHoverEvent()
    {
        isHovering = false;
    }
    public void StopSelectEvent()
    {
        isSelect = false;
    }
}
