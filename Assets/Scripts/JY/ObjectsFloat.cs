﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//건물 오브젝트들이 둥둥 떠있게 만들기
//필요속성: 트랜스폼, 움직이는 주기
public class ObjectsFloat : MonoBehaviour
{
    float currentTime;
    Vector3 originPos;
    public float frequency=1.5f;
    public float amplitude = 0.0005f;

    // Start is called before the first frame update
    void Start()
    {
        originPos = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        this.transform.position += new Vector3(0, Mathf.Sin(currentTime*frequency)*amplitude, 0);
    }
}
