﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumiditySelected : MonoBehaviour, IEventController
{
    public void HoverEvent()
    {
        
    }

    public void SelectEvent()
    {
        EJ_FloorControl.SetCultivator(true);
    }

    public void StopHoverEvent()
    {
        
    }

    public void StopSelectEvent()
    {
      
    }

}
