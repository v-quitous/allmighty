﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//이 게임오브젝트 위에 Hovering 상태이면 메터리얼의 색을 바꾸고 싶다.
//메터리얼의 색: 두 색을 정해놓고 Lerp로 왔다갔다 하기
//필요속성: 메쉬
public class CustomHighlight : MonoBehaviour, IEventController
{
    bool isHovering, isSelect;
    Material thisMat;
    Color originColor, changeColor;
    float currentTime, alphaVib;
    public float speed = 20;
    bool countAsWhite, countAsBlue;

    // Start is called before the first frame update
    void Start()
    {
        print("커스텀하이라이트");
        thisMat = GetComponent<MeshRenderer>().material;
        //thisMat.SetColor("_TintColor", Color.white);
        originColor = thisMat.color;  //(0, 196,255,199)
                                      //changeColor = new Color(1, 1, 1, 1);

        //GetComponent<Renderer>().material.color= new Color(1,1,1,0);
        countAsBlue = true;
    }

    // Update is called once per frame
    void Update()
    {

        //if(isHovering==true)
        if (Input.GetKey(KeyCode.Alpha9))
        {
            usingBool();
        }
    }

    void tuesdayStart()
    {
        //HoverHighlight();
        //안됨//this.GetComponent<MeshRenderer>().material.color = Color.Lerp(originColor, changeColor, 0.001f);
        //안됨//this.GetComponent<MeshRenderer>().material.color = Color.Lerp(originColor, Color.white, 0.001f);
        //됨//this.GetComponent<MeshRenderer>().material.color = Color.white;
        /*
        this.GetComponent<MeshRenderer>().material.color = Color.Lerp(thisMat.color, Color.white, speed*Time.deltaTime);
        this.GetComponent<MeshRenderer>().material.color = Color.Lerp(thisMat.color, originColor, speed * Time.deltaTime);
        */

        Vector4 curCol = new Vector4(thisMat.color.r, thisMat.color.g, thisMat.color.b, thisMat.color.a);
        //Vector3 curCol = new Vector3(thisMat.color.r, thisMat.color.g, thisMat.color.b);
        Vector4 white = new Vector4(1, 1, 1, 1);
        Vector4 hologram = new Vector4(0, 196 / 255f, 255 / 255f, 199 / 255f);

        if (Vector4.Distance(curCol, white) < 0.01f) //소수들끼리 계산이니까 더 작아야 할 수도 있음
        {
            print("현재 색은 흰색");
            // thisMat.color = Color.white;

            //this.GetComponent<MeshRenderer>().material.color = Color.Lerp(thisMat.color, originColor, speed); // * Time.deltaTime);
            thisMat.color = Vector4.Lerp(thisMat.color, hologram, speed * Time.deltaTime);

        }
        else if (Vector4.Distance(curCol, hologram) < 0.01f)
        {
            print("현재 색은 홀로그램");
            // countAsBlue = true;
            //this.GetComponent<MeshRenderer>().material.color = Color.Lerp(thisMat.color, Color.white, speed); // * Time.deltaTime);
            thisMat.color = Vector4.Lerp(thisMat.color, white, speed * Time.deltaTime);

        }
        else //꼬일것같으면 안써도 됨
        {
            print("지금 상태는 else");
            thisMat.color = Color.white;
        }
    }

    void usingBool()
    {
        Vector4 curCol = new Vector4(thisMat.color.r, thisMat.color.g, thisMat.color.b, thisMat.color.a);
        Vector4 white = new Vector4(1, 1, 1, 1);
        Vector4 hologram = new Vector4(0, 196 / 255f, 255 / 255f, 199 / 255f);

        if (Vector4.Distance(curCol, hologram) < 0.0001f)
        {
            print("거의 홀로그램색-> 홀로그램색");
            //thisMat.color = originColor;
            countAsWhite = false;
            countAsBlue = true;
        }
        if (countAsBlue == true)  //else if를 쓸 때와 if를 쓸 때 구분하기!
        {
            print("흰색으로 러프");
            thisMat.color = Vector4.Lerp(thisMat.color, white, speed* Time.deltaTime);
            if (Vector4.Distance(curCol, white) < 0.0001f)
            {
                print("거의 흰색-> 흰색");
                thisMat.color = Color.white;
                countAsBlue = false;
                countAsWhite = true;
            }
        }
        else if (countAsWhite == true)
        {
            print("홀로그램색으로 러프");
            thisMat.color = Vector4.Lerp(thisMat.color, hologram, speed* Time.deltaTime);
            
        }
    }

        void HovertoWhite()
        {
            this.GetComponent<MeshRenderer>().material.color = Color.Lerp(thisMat.color, Color.white, speed * Time.deltaTime);

            //안됨//if(thisMat.color-Color.white== new Color(0.1f, 0.1f, 0.1f, 0.1f))

        }

        void HovertoBlue()
        {
            this.GetComponent<MeshRenderer>().material.color = Color.Lerp(thisMat.color, originColor, speed * Time.deltaTime);
        }
        /*
         void HoverHighlight()
        {
            print("HoverHighlight 실행");
            currentTime += Time.deltaTime;
            alphaVib = Mathf.Abs(Mathf.Sin(currentTime * 3));
            //this.GetComponent<MeshRenderer>().material.color = new Color(1f, 1f, 1f,alphaVib * 0.7f);
            //안됨//this.GetComponent<MeshRenderer>().material.color = originColor+ new Color(1f, 1f, 1f,alphaVib * 0.7f);
            this.GetComponent<MeshRenderer>().material.color = new Color(0 + alphaVib * 1, 196 + alphaVib * 1, 255 + alphaVib * 1, 199 + alphaVib * 1);  
        }
        */


        public void HoverEvent()
        {
            isHovering = true;
        }
        public void SelectEvent()
        {
            isSelect = true;
        }
        public void StopHoverEvent()
        {
            isHovering = false;
        }
        public void StopSelectEvent()
        {
            isSelect = false;
        }
    }
