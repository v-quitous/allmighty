﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HighlightingSystem;

//이 게임오브젝트가 선택되면
//원래 캠퍼스의 중심으로 이동하면서
//일정 정도 커지고
//일정한 각도로 회전함
//필요속성: 원래 위치, 이동 속도, 도착지점, 원래 크기, 커질 정도, 원래 각도, 맞춰질 각도, 회전 속도

public class SelectMove : MonoBehaviour, IEventController
{
    Vector3 originPos;
    public float moveSpeed = 0.5f;
    Vector3 arrivePos;
    Vector3 originScale;
    public float scaleTimes = 5;
    Vector3 originRot;
    Vector3 arriveRot;
    public float rotSpeed = 0.5f;
    float currentTime;
    public bool isHovering, isSelect;
    GameObject campus;
    GameObject ALS;
    GameObject building200;

    Quaternion aimRot;
    bool isbuilding200Show;

    public GameObject alsSwitch;

    // Start is called before the first frame update
    void Start()
    {
        originPos = this.transform.position;
        arrivePos = GameObject.Find("ArrivePos").transform.position;
        originScale = this.transform.localScale;
        GameObject.Find("ALSwithout200_UVmapped").GetComponent<Highlighter>().enabled = true;
        GameObject.Find("ALS200_UVmapped").GetComponent<Highlighter>().enabled = true;
        campus = GameObject.Find("NewCampus");
        iconsandCampus = GameObject.Find("IconsandCampus");
        iconsandCampusOriginScale = iconsandCampus.transform.localScale;
        building200 = GameObject.Find("ALS200_UVmapped");
        ALS = GameObject.Find("ALS");
    }

    // Update is called once per frame
    void Update()
    {
        if (isHovering == true)
        {
            this.GetComponent<ObjectsFloat>().frequency = 1.7f;
            this.GetComponent<ObjectsFloat>().amplitude = 0.001f;
        }
        if (isSelect == true)
        {
            this.transform.parent = null;
            SelectEnlarge();
            PushCampusAway();
        }
    }

    public Transform pushedPos;
    public float pushSpeed;
    GameObject iconsandCampus;
    Vector3 iconsandCampusOriginScale;
    private void PushCampusAway()
    {
        iconsandCampus.transform.position = Vector3.Lerp(iconsandCampus.transform.position, pushedPos.position, pushSpeed);
        iconsandCampus.transform.localScale = Vector3.Lerp(iconsandCampus.transform.localScale, iconsandCampusOriginScale * 0.25f, pushSpeed);
    }

    public bool selectedtoEnlarge;
    public void SelectEnlarge()
    {
        this.GetComponent<ObjectsFloat>().enabled = false;
        //일정 위치로 다가오면서
        this.transform.position = Vector3.Lerp(this.transform.position, arrivePos, moveSpeed);
        //일정 정도 커짐
        this.transform.localScale = Vector3.Lerp(this.transform.localScale, originScale * scaleTimes, moveSpeed);
        //일정 각도로 회전
        this.transform.rotation = Quaternion.Lerp(this.transform.rotation, aimRot, rotSpeed);
        if (Vector3.Distance(this.transform.position, arrivePos) < 0.4f && Vector3.Distance(this.transform.localScale, originScale * scaleTimes) < 0.4f && Vector3.Distance(this.transform.eulerAngles, arriveRot) < 0.4f)
        {
            this.gameObject.SetActive(false);
            GameObject alsSwitchIns = Instantiate(alsSwitch);
            alsSwitchIns.transform.position = new Vector3(0.40946f, 2.29f, -5.58f);
            alsSwitchIns.transform.eulerAngles = new Vector3(0, -180, 0);
            alsSwitchIns.transform.localScale = new Vector3(1, 1, 1);
            EJ_ObjectPlacer.SetSelectedObject(alsSwitchIns.transform.Find("200 (1)"));
        }
    }
    public void HoverEvent()
    {
        isHovering = true;
    }
    public void SelectEvent()
    {
        isSelect = true;
    }
    public void StopHoverEvent()
    {
        isHovering = false;
    }
    public void StopSelectEvent()
    {
        isSelect = false;
    }
}

