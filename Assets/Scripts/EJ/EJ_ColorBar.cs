﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_ColorBar : MonoBehaviour
{
    public Transform upEnd;
    public Transform downEnd;
    public Transform slider;
    Color upColor;
    Color downColor;
    static float upY;
    static float downY;
    Color CurrentColor
    {
        get
        {
            return Color.Lerp(downColor, upColor, Ratio);
        }
    }

    float Ratio
    {
        get
        {
            float total = Vector3.Distance(downEnd.position, upEnd.position);
            float dist = Vector3.Distance(downEnd.position, slider.position);
            return dist / total;
        }
    }

    public static EJ_ColorBar Instance;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    void Start()
    {
        upColor = Color.red;
        downColor = Color.blue;
        upY = upEnd.position.y;
        downY = downEnd.position.y;
    }

    void Update()
    {
        SetVirtualLightColor();
    }

    public static void PlaceSlider(Vector3 pos)
    {
        Vector3 upPos = Instance.upEnd.position;
        Vector3 downPos = Instance.downEnd.position;
        Vector3 dir = pos - upPos;
        Vector3 baseDir = downPos - upPos;
        Vector3 projDir = Vector3.Project(dir, baseDir);
        Vector3 inPos = projDir + upPos;

        /*pos.x = Instance.upEnd.position.x;
        pos.z = Instance.upEnd.position.z;*/
        /*if (pos.y > upY)
            pos.y = upY;
        else if (pos.y < downY)
            pos.y = downY;*/
        if (inPos.y > upY)
            inPos = upPos;
        else if (inPos.y < downY)
            inPos = downPos;
        Instance.slider.position = inPos;
    }

    private void SetVirtualLightColor()
    {
        EJ_VirtualLightManager.SetWholeColor(CurrentColor);
    }

}
