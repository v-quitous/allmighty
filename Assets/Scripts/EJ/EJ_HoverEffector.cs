﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_HoverEffector : MonoBehaviour, IEventController
{
    public int idx;
    public void HoverEvent()
    {
        EJ_FloorHoverManager.SetSelectedIndex(idx);
    }

    public void SelectEvent()
    {

    }

    public void StopHoverEvent()
    {

    }

    public void StopSelectEvent()
    {

    }
}
