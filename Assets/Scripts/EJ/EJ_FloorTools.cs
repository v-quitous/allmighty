﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_FloorTools
{
    public static void MoveLerpTo(Transform T, Transform targetT, float lerpSpeed)
    {
        T.position = Vector3.Lerp(T.position, targetT.position, lerpSpeed * Time.deltaTime);
        T.localScale = Vector3.Lerp(T.localScale, targetT.localScale, lerpSpeed * Time.deltaTime);
    }

    public static void TransformLerpTo(Transform T, Transform targetT, float lerpSpeed)
    {
        T.position = Vector3.Lerp(T.position, targetT.position, lerpSpeed * Time.deltaTime);
        T.rotation = Quaternion.Lerp(T.rotation, targetT.rotation, lerpSpeed * Time.deltaTime);
        T.localScale = Vector3.Lerp(T.localScale, targetT.localScale, lerpSpeed * Time.deltaTime);
    }

    public static Transform CopyTransformAsChild(Transform me, Transform target)
    {
        Transform T = new GameObject().transform;
        T.position = target.position;
        T.rotation = target.rotation;
        T.parent = me;
        return T;
    }

    public static void ColorLerpTo(Material M, Color targetC, float lerpSpeed)
    {
        M.color = Color.Lerp(M.color, targetC, lerpSpeed * Time.deltaTime);
    }

    public static bool CheckApprox(Color C1, Color C2)
    {
        Vector4 v1 = new Vector4(C1.r, C1.g, C1.b, C1.a);
        Vector4 v2 = new Vector4(C2.r, C2.g, C2.b, C2.a);
        if (Vector4.Distance(v1, v2) < 0.01)
            return true;
        return false;
    }

    public static void ToggleTransform(Transform transform)
    {
        transform.gameObject.SetActive(!transform.gameObject.activeSelf);
    }

    public static void SetOffTransform(Transform transform)
    {
        transform.gameObject.SetActive(false);
    }
}
