﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_Room203 : MonoBehaviour
{
    public Transform target;
    // Update is called once per frame
    void Update()
    {
        GetKeyAndToggle();
    }

    private void GetKeyAndToggle()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            target.gameObject.SetActive(!target.gameObject.activeSelf);
        }
    }
}
