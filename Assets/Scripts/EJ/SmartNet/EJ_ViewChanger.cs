﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_ViewChanger : MonoBehaviour
{
    public Transform viewPoint;
    Transform cam;
    Vector3 origPos;
    Quaternion origRot;
    Vector3 viewPos;
    Quaternion viewRot;
    Vector3 targetPos;
    Quaternion targetRot;
    public EJ_Ray rayManager;

    public Transform realView;
    public Transform realViewLightOn;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main.transform;
        origPos = cam.position;
        origRot = cam.rotation;
        viewPos = viewPoint.position;
        viewRot = viewPoint.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        GetKeyAndChangeView();
        if (lerpOn)
            DoLerp();
    }

    bool lerpOn = false;
    float lerpSpeed = 10f;
    private void GetKeyAndChangeView()
    {
        if(Input.GetKeyDown(KeyCode.Alpha0))
        {
            realView.gameObject.SetActive(false);
            targetPos = origPos;
            targetRot = origRot;
            lerpOn = true;
        }
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            //realView.gameObject.SetActive(true);
            targetPos = viewPos;
            targetRot = viewRot;
            lerpOn = true;
        }
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(realView.gameObject.activeSelf)
            {
                realViewLightOn.gameObject.SetActive(!realViewLightOn.gameObject.activeSelf);
            }
        }
    }
    private void DoLerp()
    {
        if (rayManager.GetRayBool())
            rayManager.RayOff();
        cam.position = Vector3.Lerp(cam.position, targetPos, lerpSpeed * Time.deltaTime);
        cam.rotation = Quaternion.Lerp(cam.rotation, targetRot, lerpSpeed * Time.deltaTime);
        if (Vector3.Distance(cam.position, targetPos) < 0.1f)
        {
            cam.position = targetPos;
            cam.rotation = targetRot;
            lerpOn = false;
            rayManager.RayOn();
            if(targetPos == viewPos)
            {
                realView.gameObject.SetActive(true);
            }
        }
    }
}
