﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_VirtualLightManager : MonoBehaviour
{
    public Transform[] lights;
    public Transform cube;
    public Material mat;

    public static EJ_VirtualLightManager Instance;
    void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void Start()
    {
        //ToggleLight(false);
    }

    public void ToggleLight()
    {
        for(int i=0; i<lights.Length; i++)
        {
            lights[i].gameObject.SetActive(!lights[i].gameObject.activeSelf);
        }
    }

    public void ToggleLight(bool flag)
    {
        for (int i = 0; i < lights.Length; i++)
        {
            lights[i].gameObject.SetActive(flag);
        }
    }

    public void SetAllColor(Color color)
    {
        for (int i = 0; i < lights.Length; i++)
        {
            lights[i].GetComponent<Light>().color = color;
        }
    }

    public void SetCubeColor(Color color)
    {
        cube.gameObject.SetActive(true);
        color.a = 100 / 255f;
        mat.color = color;
    }

    public static void SetWholeColor(Color color)
    {
        //Instance.SetAllColor(color);
        Instance.SetCubeColor(color);
    }
}
