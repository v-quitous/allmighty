﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EJ_CamInfoManager : MonoBehaviour
{
    public TextMeshPro[] dates;
    public TextMeshPro[] times;
    int n;
    private void Start()
    {
        n = dates.Length;
    }

    // Update is called once per frame
    void Update()
    {
        for(int i=0; i<n; i++)
        {
            dates[i].text = System.DateTime.Now.ToString("MM/dd/yyyy").Replace("-", "/");
            times[i].text = System.DateTime.Now.ToString("HH:mm:ss:ff");
        }
    }
}
