﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_Ray : MonoBehaviour
{
    Camera cam;
    GameObject rightHand;
    LineRenderer lr;
    private void Start()
    {
        cam = Camera.main;
        rightHand = GameObject.Find("RightHand");
        lr = GetComponent<LineRenderer>();
    }

    void Update()
    {
        GetMousePos();
        //RotateHand();
        DrawRayLine();
        //Cursor.visible = false;
    }

    static Transform objectHit;
    static Vector3 hitPosition;
    private static void GetMousePos()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            objectHit = hit.transform;
            hitPosition = hit.point;
        }
        else
        {
            objectHit = null;
        }
    }

    public static Vector3 GetMouseWorldPos()
    {
        GetMousePos();
        Vector3 pos;
        if(objectHit)
        {
            pos = hitPosition;
        }
        else
        {
            print("Hit Object is null");
            pos = Vector3.zero;
        }
        return pos;
    }

    private void DrawRayLine()
    {
        lr.positionCount = 2;
        if (rightHand)
        {
            lr.SetPosition(0, rightHand.transform.position);
            lr.SetPosition(1, hitPosition);
        }
    }

    public void RayOn()
    {
        lr.enabled = true;
    }

    public void RayOff()
    {
        lr.enabled = false;
    }

    public bool GetRayBool()
    {
        return lr.enabled;
    }
}
