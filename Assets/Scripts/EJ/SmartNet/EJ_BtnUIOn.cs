﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_BtnUIOn : MonoBehaviour, IWorldUI
{
    public GameObject ui;
    public void OnClick()
    {
        ui.SetActive(!ui.activeSelf);
        gameObject.SetActive(false);
    }
}
