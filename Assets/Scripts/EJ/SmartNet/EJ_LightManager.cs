﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_LightManager : MonoBehaviour
{
    public HueLamp lamp;
    public EJ_VirtualLightManager vlm;
    public JY_ColorWheelControl colorWheel;

    private void Start()
    {
        lamp.ToggleLight(vlm.lights[0].gameObject.activeSelf);    
    }

    void Update()
    {
        UpdateColor();
    }

    private void UpdateColor()
    {
        lamp.SetColor(colorWheel.Selection);
        vlm.SetAllColor(colorWheel.Selection);
    }
}
