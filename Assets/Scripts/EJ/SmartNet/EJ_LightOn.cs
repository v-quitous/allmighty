﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_LightOn : MonoBehaviour
{
    public Transform[] lights;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            ToggleLight();
        }
    }

    public void ToggleLight()
    {
        for(int i=0; i<lights.Length; i++)
        {
            lights[i].gameObject.SetActive(!lights[i].gameObject.activeSelf);
        }
    }

}
