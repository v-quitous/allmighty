﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_BtnLightPowerOn : MonoBehaviour, IWorldUI
{
    public EJ_VirtualLightManager vlm;
    public HueLamp lamp;
    public GameObject colorWheel;
    public GameObject touchBox;

    bool lightOn = false;
    float onTime = 0;
    float onLimit = 7f;

    private void Update()
    {
        //AutoDisable();
    }

    private void AutoDisable()
    {
        onTime += Time.deltaTime;
        if (onTime > onLimit)
        {
            onTime = 0;
            transform.parent.parent.parent.gameObject.SetActive(false);
        }
    }

    public void OnClick()
    {
        lightOn = !lightOn;
        vlm.ToggleLight(lightOn);
        lamp.ToggleLight(lightOn);
        colorWheel.SetActive(lightOn);
        onTime = 0;
    }

    private void OnDisable()
    {
        vlm.ToggleLight(false);
        lamp.ToggleLight(false);
        colorWheel.SetActive(false);
        lightOn = false;
        touchBox.SetActive(true);
    }
}
