﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamControl : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("DisplayWebcam Initialize");

        var devices = WebCamTexture.devices;
        var backCamName = "";
        if (devices.Length > 0) backCamName = devices[0].name;
        for(int i=0; i<devices.Length; i++)
        {
            Debug.Log("Device: " + devices[i].name + "is front facing: " + devices[i].isFrontFacing);
            if (!devices[i].isFrontFacing)
            {
                backCamName = devices[i].name;
            }
        }
        var cameraTexture = new WebCamTexture(backCamName, 10000, 10000, 30);
        cameraTexture.Play();
        var renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = cameraTexture;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
