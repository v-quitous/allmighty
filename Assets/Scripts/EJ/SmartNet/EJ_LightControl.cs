﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_LightControl : MonoBehaviour
{
    public HueLamp lamp;
    Color lampColor;
    Color targetColor;
    public EJ_ColorWheelControl colorWheel;
    // Start is called before the first frame update
    void Start()
    {
        lampColor = Color.white;
    }

    float currentTime = 0;
    float lerpLimit = 3f;
    // Update is called once per frame
    void Update()
    {
        //CheckTimeAndChangeColor();
        UpdateColor();
    }

    private void CheckTimeAndChangeColor()
    {
        currentTime += Time.deltaTime;
        if(currentTime > lerpLimit)
        {
            lampColor = GetRandomColor();
            lamp.SetColor(lampColor);
            currentTime = 0;
        }
    }
    private void UpdateColor()
    {
        lamp.SetColor(colorWheel.Selection);
    }

    Color GetRandomColor()
    {
        return new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
    }

    public void ToggleLight()
    {
        lamp.on = !lamp.on;
    }

    public void TurnOnLight()
    {
        lamp.on = true;
    }

    public void TurnOffLight()
    {
        lamp.on = false;
    }
}
