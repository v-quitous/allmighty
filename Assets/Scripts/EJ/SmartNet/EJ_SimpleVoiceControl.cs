﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace FrostweepGames.Plugins.GoogleCloud.SpeechRecognition.Examples
{
	public class EJ_SimpleVoiceControl : MonoBehaviour
	{
		private GCSpeechRecognition _speechRecognition;

		private Image _speechRecognitionState;
		public Transform _recordState;
		private Material _recordPointMat;
		private Transform _recordText;

		private Button _startRecordButton,
					   _stopRecordButton;

		private InputField _commandsInputField;

		private Text _resultText;

		private Dropdown _languageDropdown;

		public Transform _testObject;

		private void Start()
		{
			_speechRecognition = GCSpeechRecognition.Instance;
			_speechRecognition.RecognizeSuccessEvent += RecognizeSuccessEventHandler;
			_speechRecognition.RecognizeFailedEvent += RecognizeFailedEventHandler;

			_speechRecognition.FinishedRecordEvent += FinishedRecordEventHandler;
			_speechRecognition.StartedRecordEvent += StartedRecordEventHandler;
			_speechRecognition.RecordFailedEvent += RecordFailedEventHandler;

			_speechRecognition.EndTalkigEvent += EndTalkigEventHandler;

			_startRecordButton = transform.Find("Canvas/Button_StartRecord").GetComponent<Button>();
			_stopRecordButton = transform.Find("Canvas/Button_StopRecord").GetComponent<Button>();

			_speechRecognitionState = transform.Find("Canvas/Image_RecordState").GetComponent<Image>();

			_resultText = transform.Find("Canvas/Text_Result").GetComponent<Text>();
			_recordPointMat = transform.Find("RecordState/RecordPoint").GetComponent<Renderer>().material;
			_recordText = transform.Find("RecordState/Text (TMP)");

			_commandsInputField = transform.Find("Canvas/InputField_Commands").GetComponent<InputField>();

			_languageDropdown = transform.Find("Canvas/Dropdown_Language").GetComponent<Dropdown>();

			_startRecordButton.onClick.AddListener(StartRecordButtonOnClickHandler);
			_stopRecordButton.onClick.AddListener(StopRecordButtonOnClickHandler);

			_startRecordButton.interactable = true;
			_stopRecordButton.interactable = false;
			SetRecordStateOff();

			_languageDropdown.ClearOptions();

			_speechRecognition.RequestMicrophonePermission(null);

			for (int i = 0; i < Enum.GetNames(typeof(Enumerators.LanguageCode)).Length; i++)
			{
				_languageDropdown.options.Add(new Dropdown.OptionData(((Enumerators.LanguageCode)i).Parse()));
			}

			_languageDropdown.value = _languageDropdown.options.IndexOf(_languageDropdown.options.Find(x => x.text == Enumerators.LanguageCode.ko_KR.Parse()));


			// select first microphone device
			if (_speechRecognition.HasConnectedMicrophoneDevices())
			{
                print("device is " + _speechRecognition.GetMicrophoneDevices()[0]);
                _speechRecognition.SetMicrophoneDevice(_speechRecognition.GetMicrophoneDevices()[0]);
			}
		}

		private void OnDestroy()
		{
			_speechRecognition.RecognizeSuccessEvent -= RecognizeSuccessEventHandler;
			_speechRecognition.RecognizeFailedEvent -= RecognizeFailedEventHandler;

			_speechRecognition.FinishedRecordEvent -= FinishedRecordEventHandler;
			_speechRecognition.StartedRecordEvent -= StartedRecordEventHandler;
			_speechRecognition.RecordFailedEvent -= RecordFailedEventHandler;

			_speechRecognition.EndTalkigEvent -= EndTalkigEventHandler;
		}

        private void Update()
        {
				if(OVRInput.GetDown(OVRInput.Button.One)) //A 버튼 누르고 말하기
				//if(Input.GetKeyDown(KeyCode.R))
				{
					if (_startRecordButton.interactable)
					{
							StartRecordButtonOnClickHandler();
					}
					else
					{
							StopRecordButtonOnClickHandler();
					_testObject.gameObject.SetActive(true);
					}
				}
			if (OVRInput.GetDown(OVRInput.Button.Two))
            {
				Destroy(_testObject.gameObject);
			}


		}

        private void StartRecordButtonOnClickHandler()
		{
			print("recorder works");
			_startRecordButton.interactable = false;
			_stopRecordButton.interactable = true;
			_resultText.text = string.Empty;

			_speechRecognition.StartRecord(false);
		}

		private void StopRecordButtonOnClickHandler()
		{
			_stopRecordButton.interactable = false;
			_startRecordButton.interactable = true;

			_speechRecognition.StopRecord();
		}

		private void StartedRecordEventHandler()
		{
			SetRecordStateOn();
		}

		private void RecordFailedEventHandler()
		{
			SetRecordStateOff();
			_resultText.text = "<color=red>Start record Failed. Please check microphone device and try again.</color>";

			_stopRecordButton.interactable = false;
			_startRecordButton.interactable = true;
		}

		private void EndTalkigEventHandler(AudioClip clip, float[] raw)
		{
			FinishedRecordEventHandler(clip, raw);
		}

		private void FinishedRecordEventHandler(AudioClip clip, float[] raw)
		{
			if (_startRecordButton.interactable)
			{
				SetRecordStateOff();
			}

			if (clip == null)
				return;

			RecognitionConfig config = RecognitionConfig.GetDefault();
			config.languageCode = ((Enumerators.LanguageCode)_languageDropdown.value).Parse();
			config.audioChannelCount = clip.channels;

			GeneralRecognitionRequest recognitionRequest = new GeneralRecognitionRequest()
			{
				audio = new RecognitionAudioContent()
				{
					content = raw.ToBase64()
				},
				config = config
			};

			_speechRecognition.Recognize(recognitionRequest);
		}

		private void RecognizeFailedEventHandler(string error)
		{
			_resultText.text = "Recognize Failed: " + error;
		}

		private void RecognizeSuccessEventHandler(RecognitionResponse recognitionResponse)
		{
			_resultText.text = "Detected: ";

			string[] commands = _commandsInputField.text.Split(',');

			foreach (var result in recognitionResponse.results)
			{
				foreach (var alternative in result.alternatives)
				{
					CheckAndExecute(alternative.transcript);
				}
			}
		}

        private void CheckAndExecute(string transcript)
		{
			_resultText.text += "\nIncome text: " + transcript;
			if (transcript.Contains("열어"))
			{
				if(transcript.Contains("CCTV"))
					print("given text is :" + transcript);
				_testObject.gameObject.SetActive(true);
			}
			if (transcript.Contains("켜줘"))
			{
				if (transcript.Contains("CCTV"))
					print("given text is :" + transcript);
				_testObject.gameObject.SetActive(true);
			}
			if (transcript.Contains("닫아"))
			{
				if (transcript.Contains("CCTV"))
					print("given text is :" + transcript);
				_testObject.gameObject.SetActive(false);
			}
			if (transcript.Contains("꺼줘"))
			{
				if (transcript.Contains("CCTV"))
					print("given text is :" + transcript);
				_testObject.gameObject.SetActive(false);
			}
		}

		void SetRecordStateOn()
        {
			_speechRecognitionState.color = Color.red;
			_recordPointMat.color = Color.red;
			_recordPointMat.SetColor("_EmissionColor", Color.red);
			_recordText.gameObject.SetActive(true);
			//_recordState.gameObject.SetActive(true);
		}

		void SetRecordStateOff()
        {
			_speechRecognitionState.color = Color.yellow;
			_recordPointMat.color = Color.yellow;
			_recordPointMat.SetColor("_EmissionColor", Color.yellow);
			_recordText.gameObject.SetActive(false);
			//_recordState.gameObject.SetActive(false);
		}
	}
}