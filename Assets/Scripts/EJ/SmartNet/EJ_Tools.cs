﻿using UnityEngine;

public static class EJ_Tools
{
    public static Vector3 GetHitPosWorld()
    {
        return EJ_Ray.GetMouseWorldPos();
    }

    public static Vector2 GetHitPos()
    {
        Vector3 v = EJ_Ray.GetMouseWorldPos();
        return new Vector2(v.x, v.y);
    }
}
