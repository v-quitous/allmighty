﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_FloorHoverManager : MonoBehaviour
{
    int selIdx;
    public Transform[] targets;
    float[] originY;
    int SelIdx
    {
        get
        {
            return selIdx;
        }
        set
        {
            if (value < 0 || value > 5)
                return;
            if(!locked)
                PlaceAdjuster(value);
            selIdx = value;
        }
    }

    public static EJ_FloorHoverManager Instance;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    public float yAdjuster = 0.5f;
    public float yAdder = 3;
    private void PlaceAdjuster(int value)
    {
        for (int i = 0; i < targets.Length; i++)
        {
            var pos = targets[i].position;
            pos.y = originY[i] + (5 - value) * yAdjuster;
            if (i > value)
                pos.y += yAdder;
            targets[i].position = pos;
        }
    }

    private void Start()
    {
        selIdx = 5;
        originY = new float[targets.Length];
        for(int i=0; i<targets.Length; i++)
        {
            originY[i] = targets[i].position.y;
        }
    }

    bool locked = false;
    float targetScale = 1.35f;
    private void Update()
    {
        SelectByKeyCode();
        if(!locked)
            SetSelectedScale();
    }

    private void SelectByKeyCode()
    {
        if(EJ_FloorEffectManager.EffectOn)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                SelIdx++;
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                SelIdx--;
            }
        }
    }

    float lerpSpeed = 13f;
    private void SetSelectedScale()
    {
        for(int i=0; i<targets.Length; i++)
        {
            Transform target = targets[i];
            if(i == selIdx)
            {
                target.localScale = Vector3.Lerp(target.localScale, Vector3.one * targetScale, lerpSpeed * Time.deltaTime);
            }
            else
            {
                target.localScale = Vector3.Lerp(target.localScale, Vector3.one, lerpSpeed * Time.deltaTime);
            }
        }
    }

    public static void SetSelectedIndex(int idx)
    {
        Instance.SelIdx = idx;
    }

    public static void SetLocked()
    {
        Instance.locked = true;
    }
}
