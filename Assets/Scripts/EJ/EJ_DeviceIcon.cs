﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_DeviceIcon : MonoBehaviour, IEventController
{
    public Transform iconUI;

    private void OnDisable()
    {
        EJ_FloorTools.SetOffTransform(iconUI);   
    }

    public void HoverEvent()
    {

    }

    public void SelectEvent()
    {
        EJ_FloorTools.ToggleTransform(iconUI);
        EJ_FloorControl.SetSelectedOn();
        EJ_FloorHoverManager.SetLocked();
    }

    public void StopHoverEvent()
    {

    }

    public void StopSelectEvent()
    {

    }
}
