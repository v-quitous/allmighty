﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_FloorControl : MonoBehaviour, IEventController
{
    public int idx;
    public Material matCultivator;
    public Transform[] icons;
    Color origColor;
    Color targetColor;

    public static EJ_FloorControl Instance;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        origColor = new Color(0, 217 / 255f, 1, 1);
        targetColor = new Color(1, 178 / 255f, 0, 1);
        matCultivator.color = origColor;
    }

    private void OnDestroy()
    {
        matCultivator.color = origColor;
    }

    bool filterOn = false;
    bool FilterOn
    {
        get
        {
            return filterOn;
        }
        set
        {
            filterOn = value;
            SetIcons(filterOn);
        }
    }

    private void SetIcons(bool flag)
    {
        foreach(Transform icon in icons)
        {
            icon.gameObject.SetActive(flag);
        }
    }

    float lerpSpeed = 4.5f;
    bool toOrig = false;
    bool toTarg = false;
    void Update()
    {
        if (EJ_FloorInput.FilterCultivators())
        {
            FilterOn = !FilterOn;
        }

        if(selected)
        {
            EJ_FloorTools.ColorLerpTo(matCultivator, targetColor, lerpSpeed);
            return;
        }

        if (filterOn)
        {
            if (EJ_FloorTools.CheckApprox(matCultivator.color, origColor))
            {
                print("is orig color");
                toOrig = false;
                toTarg = true;
            }
            if (EJ_FloorTools.CheckApprox(matCultivator.color, targetColor))
            {
                print("is target color");
                toOrig = true;
                toTarg = false;
            }
            if (toOrig)
            {
                EJ_FloorTools.ColorLerpTo(matCultivator, origColor, lerpSpeed);

            }
            if (toTarg)
            {
                EJ_FloorTools.ColorLerpTo(matCultivator, targetColor, lerpSpeed);

            }
        }
        else
        {
            EJ_FloorTools.ColorLerpTo(matCultivator, origColor, lerpSpeed);
        }
    }

    bool selected;

    public void HoverEvent()
    {
        EJ_FloorHoverManager.SetSelectedIndex(idx);
    }

    public void SelectEvent()
    {
        //FilterOn = !FilterOn; //JY주석처리
        GameObject targetE = GameObject.Find("TargetE"); //JY
        targetE.transform.position = new Vector3(targetE.transform.position.x+0.5f, targetE.transform.position.y, targetE.transform.position.z - 5f); //JY
    }

    public void StopHoverEvent()
    {

    }

    public void StopSelectEvent()
    {

    }

    public static void SetCultivator(bool flag)
    {
        Instance.FilterOn = flag;
    }

    public static void SetSelectedOn()
    {
        Instance.selected = true;
    }
}
