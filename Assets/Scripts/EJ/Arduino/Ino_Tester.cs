﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ino_Tester : MonoBehaviour
{
    public Text mainText;
    InoFlow Flow
    {
        get
        {
            return Ino_Manager.Instance.Flow;
        }
        set
        {
            Ino_Manager.Instance.Flow = value;
            mainText.text = "FlowStatus: " + value.ToString();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetKeyAndSetFlow();
    }

    private void GetKeyAndSetFlow()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Flow = InoFlow.IDLE;
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            Flow = InoFlow.USER;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            Flow = InoFlow.NORMAL;
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Flow = InoFlow.EMERGENCY;
        }
    }
}
