﻿#define Test

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Ino_Input
{
    /*** USER_GRAD ***/
    public static bool CultivatorOn()
    {
#if Test
        return Input.GetKeyDown(KeyCode.Alpha1);
#endif
    }
    public static bool CultivatorColor()
    {
#if Test
        return Input.GetKeyDown(KeyCode.Alpha2);
#endif
    }

    /*** MNGR_NORMAL ***/
    public static bool Normal_OneTouch()
    {
#if Test
        return Input.GetKeyDown(KeyCode.Alpha1);
#endif
    }

    /*** MNGR_EMRGNC ***/
    public static bool Emergency_OneTouch()
    {
#if Test
        return Input.GetKeyDown(KeyCode.Alpha1);
#endif
    }
    public static bool CultivatorPowerCut()
    {
#if Test
        return Input.GetKeyDown(KeyCode.Alpha2);
#endif
    }
    public static bool FirewallOn()
    {
#if Test
        return Input.GetKeyDown(KeyCode.Alpha3);
#endif
    }
}
