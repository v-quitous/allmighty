﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System;

public enum InoFlow { IDLE, USER, NORMAL, EMERGENCY };

public class Ino_Manager : MonoBehaviour
{
    public SerialPort serial = new SerialPort("COM7", 9600);
    InoFlow flow;
    public InoFlow Flow
    {
        get
        {
            return flow;
        }
        set
        {
            flow = value;
        }
    }

    public static EJ_ColorWheelControl colorWheel;
    public static Ino_Manager Instance;

    private void Awake()
    {
        if (!Instance)
            Instance = this;
    }

    private void Start()
    {
        SerialOpenCheck();
        ResetAll();
    }
    private void SerialOpenCheck()
    {
        if (serial.IsOpen == false)
            serial.Open();
    }

    static bool once0, once1, once2, once3, once4;
    private void ResetAll()
    {
        ExecCmd(Ino_Cmd.ResetAll());
        once0 = false;
        once1 = false;
        once2 = false;
        once3 = false;
        once4 = false;
    }

    public void Update()
    {
        //CheckFlowAndSetState();
    }

    private void CheckFlowAndSetState()
    {
        switch(flow)
        {
            case InoFlow.IDLE:
                UserIdle();
                break;
            case InoFlow.USER:
                UserGrad();
                break;
            case InoFlow.NORMAL:
                MngrNormal();
                break;
            case InoFlow.EMERGENCY:
                MngrEmergency();
                break;
        }
    }

    private void UserIdle()
    {

    }

    static bool done = false;
    private void UserGrad()
    {
        if (Ino_Input.CultivatorOn())
        {
            print("CultivatorOn");
            ExecCmd(Ino_Cmd.CultivatorOn(colorWheel.Selection));
        }
    }

    private void MngrNormal()
    {
        if(Ino_Input.Normal_OneTouch())
        {
            print("Normal_OneTouch");
            ExecCmd(Ino_Cmd.Normal_OneTouch());
        }
    }

    private void MngrEmergency()
    {
        if(Ino_Input.Emergency_OneTouch())
        {
            print("Emergency_OneTouch");
            ExecCmd(Ino_Cmd.Emergency_OneTouch());
        }
        if(Ino_Input.CultivatorPowerCut())
        {
            print("CultivatorPowerCut");
            ExecCmd(Ino_Cmd.CultivatorPowerCut());
        }
        if(Ino_Input.FirewallOn())
        {
            print("FirewallOn");
            ExecCmd(Ino_Cmd.FireWallOn());
        }
    }

    void ExecCmd(string cmd)
    {
        serial.Write(cmd);
    }

    public static void CultivatorOn()
    {
        if(!once0)
        {
            print("CultivatorOn");
            Instance.ExecCmd(Ino_Cmd.CultivatorOn(colorWheel.Selection));
            once0 = true;
        }

    }

    public static void Normal_OneTouch()
    {
        if(!once1)
        {
            print("Normal_OneTouch");
            Instance.ExecCmd(Ino_Cmd.Normal_OneTouch());
            once1 = true;
        }
    }

    public static void Emergency_OneTouch()
    {
        if(!once2)
        {
            print("Emergency_OneTouch");
            Instance.ExecCmd(Ino_Cmd.Emergency_OneTouch());
            once2 = true;
        }
    }

    public static void CultivatorPowerCut()
    {
        if(!once3)
        {
            print("CultivatorPowerCut");
            Instance.ExecCmd(Ino_Cmd.CultivatorPowerCut());
            once3 = true;
        }
    }

    public static void FirewallOn()
    {
        if(!once4)
        {
            print("FirewallOn");
            Instance.ExecCmd(Ino_Cmd.FireWallOn());
            once4 = true;
        }
    }
}
