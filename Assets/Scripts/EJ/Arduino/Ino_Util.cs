﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Ino_Util
{
    public static string GetCultivatorColorSetting(Color sel)
    {
        int r = (int)sel.r * 255;
        int g = (int)sel.g * 255;
        int b = (int)sel.b * 255;
        //return "cultivator-left-set_whole_color-" + r + "-" + g + "-" + b;
        return "cultivator-left-set_whole_color-" + 100 + "-" + 0 + "-" + 100;
    }
}
