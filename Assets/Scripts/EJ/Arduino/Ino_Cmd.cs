﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Ino_Cmd
{
    internal static string ResetAll()
    {
        string cmd = "";
        cmd += "cultivator-left-off";
        cmd += "|";
        cmd += "door_lock-lock";
        cmd += "|";
        cmd += "window-close";
        cmd += "|";
        cmd += "lobby_light_left-off";
        cmd += "|";
        cmd += "lobby_light_right-off";
        cmd += "|";
        cmd += "fire_warning_light-off";
        cmd += "|";
        cmd += "elevator-stop";
        cmd += "|";
        cmd += "parking_gate-close";
        cmd += "|";
        cmd += "firewall-deactivate";
        return cmd;
    }

    public static string CultivatorOn(Color color)
    {
        string cmd = "";
        // 새싹재배기 켜기
        cmd += "cultivator-left-on";
        cmd += "|";
        cmd += Ino_Util.GetCultivatorColorSetting(color);
        return cmd;
    }

    internal static string Normal_OneTouch()
    {
        string cmd = "";
        // 출입문 잠금해제
        cmd += "door_lock-unlock";
        // 창문열기
        cmd += "|";
        cmd += "window-open";
        // 중앙과 로비 조명 켜기
        cmd += "|";
        cmd += "lobby_light_left-on";
        cmd += "|";
        cmd += "lobby_light_left-set_whole_color-100-100-100";
        cmd += "|";
        cmd += "lobby_light_right-on";
        cmd += "|";
        cmd += "lobby_light_right-set_whole_color-100-100-100";
        return cmd;
    }

    internal static string Emergency_OneTouch()
    {
        string cmd = "";
        // 화재알람
        cmd += "fire_warning_light-fade-100";
        // 대피조명 켜기
        cmd += "|";
        cmd += "lobby_light_left-set_whole_color-100-100-30";
        // 엘리베이터 중단
        cmd += "|";
        cmd += "elevator-stop";
        // 주차 차단기 열기
        cmd += "|";
        cmd += "parking_gate-open";
        cmd += "|";
        cmd += "cultivator-left-off";
        cmd += "|";
        cmd += "firewall-activate";
        return cmd;
    }

    internal static string CultivatorPowerCut()
    {
        // 새싹재배기 전원차단
        return "cultivator-left-off";
    }

    internal static string FireWallOn()
    {
        // 방화벽 작동
        return "firewall-activate";
    }
}
