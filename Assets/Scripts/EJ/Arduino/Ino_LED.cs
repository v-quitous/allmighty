﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;

public class Ino_LED : MonoBehaviour
{
    public SerialPort serial = new SerialPort("COM6", 9600);
    bool lightState = false;

    public void Update()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            print("turnOn LED");
            onLed();
        }
        if (Input.GetKey(KeyCode.Alpha0))
        {
            print("turnOff LED");
            offLed();
        }
    }

    public void onLed()
    {
        if(serial.IsOpen == false)
        {
            serial.Open();
        }
        serial.Write("A");
        lightState = true;
    }

    public void offLed()
    {
        if(serial.IsOpen == false)
        {
            serial.Open();
        }
        serial.Write("a");
        lightState = false;
    }
}
