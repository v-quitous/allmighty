﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EJ_FloorInput
{
    public static bool SeparateBool()
    {
        return Input.GetKeyDown(KeyCode.Alpha1);
    }

    public static bool FilterCultivators()
    {
        return Input.GetKeyDown(KeyCode.Alpha2);
    }
}
