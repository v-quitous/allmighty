﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_BarSlider : MonoBehaviour, IEventController
{
    bool selected = false;
    void Update()
    {
        if(selected)
        {
            if(EJ_EventManager.GetRHitPos() != null)
            {
                Vector3 pos = (Vector3)EJ_EventManager.GetRHitPos();
                EJ_ColorBar.PlaceSlider(pos);
            }
        }
    }

    public void HoverEvent()
    {

    }

    public void SelectEvent()
    {
        selected = true;
    }

    public void StopHoverEvent()
    {

    }

    public void StopSelectEvent()
    {
        selected = false;
    }
}
