﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_FloorEffectManager : MonoBehaviour, IEventController
{
    public Transform[] floors;
    Transform[] floorOrigins;
    public Transform [] targets;
    BoxCollider boxCol;

    static bool effectOn = false;
    public static bool EffectOn
    {
        get
        {
            return effectOn;
        }
        set
        {
            effectOn = value;
        }
    }
    int n;

    // Start is called before the first frame update
    void Start()
    {
        n = floors.Length;
        floorOrigins = new Transform[n];
        for(int i = 0; i < n; i++)
        {
            floorOrigins[i] = EJ_FloorTools.CopyTransformAsChild(transform, floors[i]);
        }
        boxCol = GetComponent<BoxCollider>();
    }

    float lerpSpeed = 15f; //JY
    // Update is called once per frame
    void Update()
    {
        if (EJ_FloorInput.SeparateBool())
        {
            effectOn = !effectOn;
        }

        if (effectOn)
        {
            for (int i = 0; i < n; i++)
            {
                EJ_FloorTools.MoveLerpTo(floors[i], targets[i], lerpSpeed);
            }
        }
        else
        {
            for (int i = 0; i < n; i++)
            {
                EJ_FloorTools.MoveLerpTo(floors[i], floorOrigins[i], lerpSpeed);
            }
        }
    }

    public void HoverEvent()
    {

    }

    public void SelectEvent()
    {
        EffectOn = !EffectOn;
        EJ_ObjectPlacer.EffectOn = !EJ_ObjectPlacer.EffectOn;
        boxCol.enabled = !boxCol.enabled;
    }

    public void StopHoverEvent()
    {

    }

    public void StopSelectEvent()
    {

    }
}
