﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EJ_ObjectPlacer : MonoBehaviour
{
    public Transform selObj;
    Transform originPos;
    public Transform targetPos;

    public static EJ_ObjectPlacer Instance;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void Start()
    {
        //originPos = EJ_FloorTools.CopyTransformAsChild(transform, selObj);
    }

    static bool effectOn = false;
    public static bool EffectOn
    {
        get
        {
            return effectOn;
        }
        set
        {
            effectOn = value;
        }
    }

    float lerpSpeed = 20f;
    // Update is called once per frame
    void Update()
    {
        if (EJ_FloorInput.SeparateBool())
        {
            effectOn = !effectOn;
        }

        if (effectOn)
        {
            EJ_FloorTools.TransformLerpTo(selObj, targetPos, lerpSpeed);
        }
        else
        {
            if(originPos)
                EJ_FloorTools.TransformLerpTo(selObj, originPos, lerpSpeed);
        }
    }

    public static void SetSelectedObject(Transform objectT)
    {
        Instance.selObj = objectT;
        Instance.originPos = EJ_FloorTools.CopyTransformAsChild(Instance.transform, Instance.selObj);
    }
}
