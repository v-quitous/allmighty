﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//중지를 누르면 Hover(=Ray쏘고 있는 상태)
//중지 누른 상태에서 검지도 누르면 Select

public class EJ_EventManager : MonoBehaviour
{
    GameObject lHand, rHand;

    Ray lRay, rRay;
    public RaycastHit lHitInfo, rHitInfo;
    IEventController eventController;
    
    bool isLHandHovering, isRHandHovering, isLHandSelect, isRHandSelect;
    bool isLHandRayBeenCasted, isRHandRayBeenCassted;

    LineRenderer llr, rlr;

    public static EJ_EventManager Instance;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        lHand = GameObject.Find("LeftHandAnchor");
        rHand = GameObject.Find("RightHandAnchor");
        llr = lHand.GetComponent<LineRenderer>();
        llr.enabled = false;
        rlr = rHand.GetComponent<LineRenderer>();
        rlr.enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        lHandRay();
        lHandSelect();
        rHandRay();
        rHandSelect();
    }

    private void DrawLRayLine()
    {
        llr.enabled = true;
        llr.positionCount = 2;
        llr.SetPosition(0, lHand.transform.position);
        llr.SetPosition(1, lHitInfo.point);
    }
    private void DrawRRayLine()
    {
        rlr.enabled = true;
        rlr.positionCount = 2;
        rlr.SetPosition(0, rHand.transform.position);
        rlr.SetPosition(1, rHitInfo.point);
    }

    private void lHandRay()
    {
        if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.LTouch) != 0)
        {
            lRay = new Ray(lHand.transform.position, lHand.transform.forward);
            DrawLRayLine();
            if (Physics.Raycast(lRay, out lHitInfo))
            {
                eventController = lHitInfo.transform.gameObject.GetComponent<IEventController>();
                print(" 왼손레이 eventController:" + eventController);
                print(" 왼손레이 오브젝트:" + lHitInfo.transform.gameObject.name);
                //IEventController eventController = lHitInfo.transform.gameObject.GetComponent<IEventController>();
                if (eventController != null)
                {
                    isLHandHovering = true;
                    eventController.HoverEvent();
                    print("왼쪽으로 HoverEvent 실행");
                }
            }
        }

        if (eventController!= null&& OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.LTouch))
        {
            llr.enabled = false;
            isLHandHovering = false;
            eventController.StopHoverEvent();
            print("왼쪽으로 StopHoverEvent 실행");
            eventController = null;
        }
    }

    private void lHandSelect()
    {
        if (isLHandHovering == true && OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.LTouch))
        {
            if (eventController != null)
            {
                isLHandSelect = true;
                eventController.SelectEvent();
                print("왼쪽으로 SelectEvent 실행");
            }
        }
        if(isLHandHovering== true&& OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.LTouch))
        {
            isLHandSelect = false;
            eventController.StopSelectEvent();
            print("왼쪽으로 StopSelectEvent 실행");
            eventController.StopHoverEvent();
            eventController = null;
        }
      
    }

    private void rHandRay()
    {
        if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.RTouch) != 0)

        {
            rRay = new Ray(rHand.transform.position, rHand.transform.forward);
            DrawRRayLine();
            if (Physics.Raycast(rRay, out rHitInfo))
            {
                eventController = rHitInfo.transform.gameObject.GetComponent<IEventController>();
                print(" 오른손레이 eventController:" + eventController);
                print(" 오른손레이 오브젝트:" + rHitInfo.transform.gameObject.name);
                if (eventController != null)
                {
                    isRHandHovering = true;
                    eventController.HoverEvent();
                    print("오른쪽으로 HoverEvent 실행");
                }
            }
        }
        if (eventController!= null && OVRInput.GetUp(OVRInput.Button.PrimaryHandTrigger, OVRInput.Controller.RTouch))
        {
            rlr.enabled = false;
            isRHandHovering = false;
            eventController.StopHoverEvent();
            print("오른쪽으로 StopHoverEvent 실행");
            eventController = null;
        }
    }

    private void rHandSelect()
    {
        if (isRHandHovering == true && OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch))
        {
            if (eventController != null)
            {
                isRHandSelect = true;
                eventController.SelectEvent();
                print("오른쪽으로 SelectEvent 실행");
            }
        }
        if (isRHandHovering==true&& OVRInput.GetUp(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch))
        {
            isRHandSelect = false;
            eventController.StopSelectEvent();
            eventController.StopHoverEvent();
            print("오른쪽으로 StopSelectEvent 실행");
            eventController = null;
        }
       
    }

    public static Vector3? GetRHitPos()
    {
        if (Instance.rHitInfo.transform)
            return Instance.rHitInfo.point;
        return null;
    }
}
